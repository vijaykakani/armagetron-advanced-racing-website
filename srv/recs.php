#!/usr/bin/php
<?php
date_default_timezone_set('Etc/GMT');

$recs = dirname(__DIR__)."/var/race_scores/";

$race_year = 2013;

$current_year = date("Y");
$year_difference = $current_year - $race_year;
if ($year_difference > 0)
{
    $backups_dir = $recs."__backups/";
    if (!is_dir($backups_dir))
        mkdir($backups_dir, 0777);
    chmod($backups_dir, 0777);

    $backups_dir .= $race_year."/";
    if (!is_dir($backups_dir))
        mkdir($backups_dir, 0777);
    chmod($backups_dir, 0777);

    foreach (scandir($recs) as $rec)
    {
        if (is_dir($recs . $rec))
        {
            if ($rec == "__backups") continue;
            if ($rec == ".") continue;
            if ($rec == "..") continue;

            rename($recs . "/" . $rec, $backups_dir . $rec);
        }
    }

    con('0xff7700[ 0x00ccccSERVER SCRIPT 0xff7700] 0xffff7f: Records have been reset for '.$current_year);

    $race_year = $current_year;
}