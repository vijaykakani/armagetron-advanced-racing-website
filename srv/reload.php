<pre>
<?php
require dirname(__DIR__)."/base.php";

$recs_path_new = Data::$data->root."/new_race_scores/"; //  path to new records to be saved in
$recs_path_old = Data::$data->root."/old_race_scores/"; //  path to old records to be stored in

foreach (Data::$data->resources as $resource)
{
    $path = Data::$data->rec_path."/".$resource."/";
    foreach (scandir($path) as $file)
    {
        //  skip if it is not a file
        if (!is_file($path.$file))
            continue;

        $records  = array();

        //  fetch the original records
        $recs     = explode("\n", trim(file_get_contents($path.$file)));

        //  fetch the new records (named as old)
        $old_recs = explode("\n", trim(file_get_contents($recs_path_old.$resource."/".$file)));

        //  show the preview of the original and new
        echo "<hr />";
        echo $resource."/".$file."\n";
        echo htmlentities(print_r($recs, true));
        echo htmlentities(print_r($old_recs, true));

        foreach ($old_recs as $old_rec)
        {
            //  explode the string to get PLAYER  TIME FINISHED <...>
            $str = explode(" ", $old_rec);

            //  get the index key of the player from original record
            $id_key = GetPlayerIndex($str[0], $recs);

            //  check if the index key exists
            if ($id_key !== false)
            {
                //  if the compare returns true
                if (CompareSets($str, explode(" ", $recs[$id_key])))
                    $recs[$id_key] = $old_rec;  //  replace original with new
            }
            //  add record if it does not exist in original
            else $recs[] = $old_rec;
        }

        //  set them into the $records by their time as index
        foreach ($recs as $rec)
            $records[explode(" ", $rec)[1]] = $rec;

        //  sort out the $records by their key (time)
        ksort($records);

        //  show how the records are arranged
        echo htmlentities(count($records).print_r($records, true));
        echo "<hr />\n";

        //  add the content to the files
        if (!is_dir($recs_path_new.$resource))
            mkdir($recs_path_new.$resource, "0755", true);
        file_put_contents($recs_path_new.$resource."/".$file, implode("\n", $records));
    }
}

function GetPlayerIndex($player, $recs)
{
    foreach ($recs as $key => $rec)
    {
        $str = explode(" ", $rec);
        if ($str[0] == $player)
            return $key;
    }
    return false;
}

function CompareSets($old, $new)
{
    //  the change in time
    if ($old[1] != $new[1])
        return true;

    //  the change in times finished
    if ($old[2] != $new[2])
        return true;

    return false;
}
?>
</pre>