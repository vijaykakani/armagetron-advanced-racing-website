#!/usr/bin/php
<?php
set_error_handler('MyErrorHandler');                //  the function to handle errors catches
register_shutdown_function('MyFatalErrorHandler');  //  the function to handle fatal error sand shutdown of script

date_default_timezone_set('Etc/GMT');   //  set the default timezone

$streamer = STDIN;
//$streamer = fopen('../logs/ladderlog.txt', 'r');

$website = "loverboysracing.tk";
$recs = dirname(__DIR__)."/var/race_scores/";
$cfgs = dirname(__DIR__)."/config/Modes/";

$race_year = date("Y"); //  the race year the records belong to

//  Get the list of maps from the config file
$maps = explode(';', str_replace("MAP_ROTATION ", "", getLineWithString($cfgs."racing.cfg", "MAP_ROTATION")));

$map    = "";       //  used to contain the map name
$map_id = false;    //  holds the key of the maps

$SP     = false;    //  flag to indicate whether it is single player or not
$SP_SET = false;    //  flag to indicate if single player mode is set or not

con('0xff7700--- [ 0x00ccccSERVER SCRIPT ... 0x00de00ONLINE 0xff7700] ---');

while (!feof($streamer))
{
    try
    {
        $line = trim(fgets($streamer, 2048));
        $parts = explode(' ', $line);

        switch ($parts[0])
        {
            //  CURRENT_MAP [size_factor] [size_multiplier] [MAP_FILE]
            case "CURRENT_MAP":
                $map    = $parts[3];
                $map_id = array_search($map, $maps);
                break;

            //  ROUND_ENDED [time]
            case "ROUND_ENDED":
                UpdateWebsite($race_year, $map);

                echo "CLEAR_LADDERLOG\n";

                $map    = "";
                $map_id = false;

                $SP_SET = false;

                //  skip the records from updating
                $current_year = date("Y");
                $year_difference = $current_year - $race_year;
                if ($year_difference == 1)
                {
                    $backups_dir = $recs."__backups/".$race_year."/";
                    if (!is_dir($backups_dir))
                        mkdir($backups_dir, 0777, true);

                    foreach (scandir($recs) as $rec)
                    {
                        if (is_dir($recs . $rec))
                        {
                            if ($rec == "__backups") continue;
                            if ($rec == ".") continue;
                            if ($rec == "..") continue;

                            rename($recs . "/" . $rec, $backups_dir . $rec);
                        }
                    }

                    con('0xff7700[ 0x00ccccSERVER SCRIPT 0xff7700] 0xffff7f: Records have been reset for '.$current_year);

                    $race_year = $current_year;
                }
                break;

            //  ONLINE_PLAYERS_COUNT <humans> <ais> <humans alive> <ai alive> <humans dead> <ai dead>
            case "ONLINE_PLAYERS_COUNT":
                $humans       = $parts[1];
                $ais          = $parts[2];
                $humans_alive = $parts[3];
                $ais_alive    = $parts[4];
                $humans_dead  = $parts[5];
                $ais_dead     = $parts[6];

                if (!$SP_SET)
                {
                    if ($humans >= 1)
                    {
                        //  if there is only one
                        if (($humans_alive == 1) && !$SP)
                        {
                            $SP = true;                     //  it is single player mode
                            echo "QUEUE_LIMIT_ENABLED 0\n"; //  disable queue limit
                            con('0xff7700[ 0x00ccccSERVER SCRIPT 0xff7700] 0xffff7f: Single Player Activated');
                        }

                        //  if there is more or less than 1
                        if (($humans_alive != 1) && $SP)
                        {
                            $SP = false;                    //  it is not a single player mode
                            echo "QUEUE_LIMIT_ENABLED 1\n"; //  enable queue limit

                            //  restore all other settings back to normal
                            rotationType(5);
                            raceTimer(1);
                            raceSave(1);

                            con('0xff7700[ 0x00ccccSERVER SCRIPT 0xff7700] 0xffff7f: Single Player Disabled');
                        }
                    }

                    $SP_SET = true;
                }
                break;

            //  GAME_END [time]
            case "GAME_END":
                $rec_files = array();

                foreach ($iterator = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($recs,
                        RecursiveDirectoryIterator::SKIP_DOTS),
                    RecursiveIteratorIterator::SELF_FIRST) as $item) {
                    // Note SELF_FIRST, so array keys are in place before values are pushed.

                    $subPath = $iterator->getSubPathName();

                    //  we don't need to go through the backup files
                    if (strpos($subPath, "__backups"))
                        continue;

                    if(!$item->isDir())
                    {
                        // Add a new element to the array of the current file name.
                        $rec_files[$subPath][] = $subPath;
                    }
                }

                //  get rid of indexes of empty values
                $rec_files = array_filter($rec_files);

                foreach ($rec_files as $file)
                {
                    $map = substr($file[0], 0, -4);
                    UpdateWebsite($race_year, $map, false);
                    sleep(2);
                }

                echo "SERVER_PORT 4537\n";
                break;

            //  INVALID_COMMAND [command] [player_username] [ip_address] [access_level] [params]
            case "INVALID_COMMAND":
                if ($SP)
                {
                    switch (substr($parts[1], 1))
                    {
                        //  starts the map rotation
                        case 'start':
                            rotationType(5);
                            con('0xff7700[ 0x00ccccSERVER SCRIPT 0xff7700] 0xffff7f: Rotation back to normal.');
                            break;

                        //  skips the map to the next
                        case 'skip':
                            if ($map_id !== false)
                            {
                                $new_map_id = $map_id + 1;
                                if ($new_map_id >= count($maps))
                                    $new_map_id = 0;
                                $map_id = false;

                                killAll();
                                echo "MAP_ROTATION_LOAD " . $new_map_id . "\n";

                                con('0xff7700[ 0x00ccccSERVER SCRIPT 0xff7700] 0xffff7f: Skipping map.');
                            }
                            break;

                        //  stops the map rotation
                        case 'stop':
                            rotationType(0);
                            con('0xff7700[ 0x00ccccSERVER SCRIPT 0xff7700] 0xffff7f: Stopped rotation.');
                            break;

                        //  freezes the race timer (disable)
                        case 'freeze':
                            raceTimer(0);
                            raceSave(0);
                            con('0xff7700[ 0x00ccccSERVER SCRIPT 0xff7700] 0xffff7f: Race frozen.');
                            break;

                        //  unfreezes the race timer (enable)
                        case 'unfreeze':
                            raceTimer(1);
                            raceSave(1);
                            con('0xff7700[ 0x00ccccSERVER SCRIPT 0xff7700] 0xffff7f: Race unfrozen.');
                            break;

                        //  returns everything back to normal
                        case 'restore':
                            rotationType(5);
                            raceTimer(1);
                            raceSave(1);
                            con('0xff7700[ 0x00ccccSERVER SCRIPT 0xff7700] 0xffff7f: Restored.');
                            break;

                        default:
                            cpm($parts[2], 'chat_command_unknown', $parts[1]);
                            break;
                    }
                }
                else cpm($parts[2], 'chat_command_unknown', $parts[1]);
                break;

            default:
                break;
        }
    }
    catch (ErrorException $ex)
    {
        con('0xff7700[ 0x00ccccSERVER SCRIPT 0xff7700] 0xffff7f: '.$ex->getMessage());
    }
}

//  handles errors but not fatal errors
function MyErrorHandler($errno, $errstr, $errfile, $errline)
{
    throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
}

//  handles fatal errors and showdown of script
function MyFatalErrorHandler()
{
    con('0xff7700--- [ 0x00ccccSERVER SCRIPT ... 0xff9999OFFLINE 0xff7700] ---');

    $last_error = error_get_last();
    if ($last_error)
        con('0xff7700[ 0x00ccccSERVER SCRIPT 0xff7700] 0xffff7f: '.$last_error['message']);

}

function getLineWithString($fileName, $str)
{
    $lines = file($fileName);
    foreach ($lines as $lineNumber => $line)
    {
        if (strpos($line, $str) !== false)
            return $line;
    }
    return false;
}

function UpdateWebsite($year, $map, $announce = true)
{
    global $recs, $website;

    //  skip the map updating when there is no map selected
    if (trim($map) == "") return;

    $path = $recs . $map . ".txt";
    //echo "CONSOLE_MESSAGE 0x99ff99PATH: ".$path."\n";

    if (!file_exists($path)) return;
    //echo "CONSOLE_MESSAGE 0x009900EXISTS: 0x00ff00TRUE\n";

    $contents = file_get_contents($path);
    if (trim($contents) == '') return;
    //echo "CONSOLE_MESSAGE 0x009900CONTENT: 0x00ff00TRUE\n";

    $data = urlencode(htmlentities($contents));

    $url = "http://".$website."/scripts/get.php?year=" . $year . "&map=" . $map . "&data=" . $data;
    file_get_contents($url);

    if ($announce)
        con('0xff7700[ 0x00ccccSERVER SCRIPT 0xff7700] 0xffff7f: Records have been updated to http://loverboysracing.tk');
}

function con($msg)                  { echo "CONSOLE_MESSAGE ".$msg."\n"; }
function pm($player, $msg)          { echo "PLAYER_MESSAGE ".$player.' "'.$msg.'"'."\n"; }
function cpm($player, $lang, $msg)  { echo "CUSTOM_PLAYER_MESSAGE ".$player." ".$lang." ".$msg."\n"; }
function killAll()                  { echo "KILL_ALL\n"; }
function raceTimer($v)              { echo "RACE_TIMER_ENABLED ".$v."\n"; }
function raceSave($v)               { echo "RACE_RECORDS_SAVE ".$v."\n"; }
function rotationType($v)           { echo "ROTATION_TYPE ".$v."\n"; }