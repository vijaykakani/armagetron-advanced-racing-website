<?php

class Wall
{
    var $points = array();
}

class Zone
{
    var $x;
    var $y;
    var $radius;
    var $color;
    var $effect;
}

class Spawn
{
    var $x;
    var $y;
    var $xdir;
    var $ydir;
    var $angle;
}

function MapViewer($path_to_map, $map_file, $path_to_svg, $svg_name, $size)
{
    //!Constants
    $filePath = $path_to_map."/".$map_file;
    $fileSave = $path_to_svg."/".$svg_name.".svg";

    $margin = 5;
    $stroke = 2;

    $size_multiplier = 1.0;
    $size_factor     = 0.0;

    //  leave these as they are as they will get adjusted later on
    $minx = 10000;
    $maxx = -10000;
    $miny = 10000;
    $maxy = -10000;

    //!Post-Close Declarations
    $wallList  = array();
    $zoneList  = array();
    $spawnList = array();

    $dom = new DOMDocument;
    $dom->load($filePath);

    $settings = $dom->getElementsByTagName('Setting');
    if (!is_null($settings))
    {
        foreach ($settings as $setting)
        {
            $name  = $setting->getAttribute("name");
            $value = $setting->getAttribute("value");

            if (Filter($name) == "size_factor")
            {
                $size_factor     = floatval($value);
                $size_multiplier = exponent($size_factor);
            }
        }
    }

    $walls = $dom->getElementsByTagName('Wall');
    if (!is_null($walls))
    {
        foreach($walls as $wall)    // load each wall
        {
            //  create new instance of a new wall
            $newWall = new Wall();

            $points = $wall->getElementsByTagName('Point'); //get their points
            $wPointer = '';
            foreach($points as $point)
            {
                $xData = $point->getAttribute("x") * $size_multiplier;     // get the x value
                $yData = -$point->getAttribute("y") * $size_multiplier;    // get the y value

                // this is to adjust the view of the map. Leave it as it is!
                if($minx > $xData) $minx = $xData;
                if($maxx < $xData) $maxx = $xData;
                if($miny > $yData) $miny = $yData;
                if($maxy < $yData) $maxy = $yData;

                //  place data into class array
                $newWall->points[] = $xData;
                $newWall->points[] = $yData;

                $wPointer .= $xData.','.$yData.' '; // store them in the variable
            }

            //  store wall data into previously declared wall array
            $wallList[] = $newWall;
        }
    }

    $zones = $dom->getElementsByTagName('Zone');
    if (!is_null($zones))   // if zones do exist on the map
    {
        foreach($zones as $zone)    // load each zone
        {
            //  create new instance of a new zone
            $newZone = new Zone();

            $color = '#ffa500';
            $effect = $zone->getAttribute('effect');    // get the effect type
            if (!is_null($effect) && $effect == 'win')  // if the effect it that
                $color = '#33ff33'; // set the color suited for that type
            elseif(!is_null($effect) && $effect == 'death')
                    $color = '#3399ff';
            elseif(!is_null($effect) && $effect == 'flag')
                    $color = '#0000ff';
            elseif(!is_null($effect) && $effect == 'ball')
                    $color = 'brown';
            elseif(!is_null($effect) && $effect == 'rubber')
                    $color = '#ffdd00';
            elseif(!is_null($effect) && $effect == 'target')
                    $color = '#00ff99';
            elseif(!is_null($effect) && $effect == 'teleport')
                    $color = '#0055ff';

            $newZone->effect = $effect;

            //  set the color of the zone
            $newZone->color = $color;

            $shapes = $zone->getElementsByTagName('ShapeCircle');

            if ($shapes)    // if that shape exists in the map file
            {
                foreach($shapes as $shape)  // load each shape
                {
                    // get the shape's radius
                    $newZone->radius = $shape->getAttribute('radius') * $size_multiplier;

                    // get it's points
                    $points = $shape->getElementsByTagName('Point');

                    foreach($points as $point)      // for each point (usually be only 1 point)
                    {
                        $newZone->x = $point->getAttribute('x') * $size_multiplier;    // get the x value
                        $newZone->y = -$point->getAttribute('y') * $size_multiplier;   // get the y value
                    }

                    $zoneList[] = $newZone;

                    //  just in case some idiot puts in more ShapeCircles, let's break out after the first go
                    break;
                }
            }
            else continue;
        }
    }

    $spawns = $dom->getElementsByTagName('Spawn');
    if (!is_null($spawns))      // if spawns do exist on the map
    {
        foreach($spawns as $spawn)      // load each spawn
        {
            $newSpawn = new Spawn();

            //  get and x and y coordinates and store them in data
            $newSpawn->x = $spawn->getAttribute('x') * $size_multiplier;
            $newSpawn->y = -$spawn->getAttribute('y') * $size_multiplier;

            if ($spawn->hasAttribute('angle'))
            {
                $newSpawn->angle = $spawn->getAttribute('angle');
            }
            else
            {
                $newSpawn->xdir = $spawn->getAttribute('xdir');
                $newSpawn->ydir = $spawn->getAttribute('ydir');
            }

            $spawnList[] = $newSpawn;
        }
    }

    //! Write the initial code for beginning the svg drawings

    $sizeWidth = $size;         // the width of the map to display on the browser screen
    $sizeHeight = $size;        // the height of the map to display on the browser screen
    /*  You can replace the above values with manual values like:
            $sizeWidth = '1366px';
            $sizeHeight = '500px';
        Taking the ones above as examples, it's up to you now.
    */

    $svg = new DOMDocument("1.0", "UTF-8");
    $svg->formatOutput = true;
    $svg->xmlStandalone = false;

    $svg_root = $svg->createElement('svg');

    $viewbox = (($minx - $margin).','.($miny - $margin).','.($maxx - $minx + 2*$margin).','.($maxy - $miny + 2*$margin));
    buildSvg($svg, $svg_root, $viewbox, $sizeWidth, $sizeHeight);

    //! Create elements to hold all the wall data
    if (count($wallList) > 0)
    {
        foreach ($wallList as $wallData)
        {
            //  store lines in an SVG readable format
            $points = '';
            $first = true;
            for ($i = 0; $i < count($wallData->points); $i += 2)
            {
                if (!$first)
                    $points .= " ".$wallData->points[$i].",".$wallData->points[$i + 1];

                if ($first)
                {
                    $points .= $wallData->points[$i].",".$wallData->points[$i + 1];
                    $first = false;
                }
            }

            //  create the polyline for walls
            $polyline = $svg->createElement("polyline");

            //  the points that hold the location for each line points
            $point_s = $svg->createAttribute("points");
            $point_s->value = $points;
            $polyline->appendChild($point_s);

            //  color of line
            $stroke_t = $svg->createAttribute("stroke");
            $stroke_t->value = "white";
            $polyline->appendChild($stroke_t);

            //  width of line
            $stroke_width = $svg->createAttribute("stroke-width");
            $stroke_width->value = $stroke;
            $polyline->appendChild($stroke_width);

            //  no need to fill the space created by the connected lines
            $fill = $svg->createAttribute("fill");
            $fill->value = "none";
            $polyline->appendChild($fill);

            //  append the polyline to the main svg
            $svg_root->appendChild($polyline);
        }
    }

    //  Create elements to hold all the zones data
    if (count($zoneList) > 0)
    {
        foreach ($zoneList as $zoneData)
        {
            //  create the circle for zones
            $circle = $svg->createElement("circle");

            //  the center x location of circle
            $x_pos = $svg->createAttribute("cx");
            $x_pos->value = $zoneData->x;
            $circle->appendChild($x_pos);

            //  the center y location of circle
            $y_pos = $svg->createAttribute("cy");
            $y_pos->value = $zoneData->y;
            $circle->appendChild($y_pos);

            //  circle radius
            $radius = $svg->createAttribute("r");
            $radius->value = $zoneData->radius;
            $circle->appendChild($radius);

            //  color of circle outline
            $stroke_t = $svg->createAttribute("stroke");
            $stroke_t->value = $zoneData->color;
            $circle->appendChild($stroke_t);

            //  by the factor circle should appear visible by: 1-full;0-none
            $opacity = $svg->createAttribute("opacity");
            $opacity->value = "1";
            $circle->appendChild($opacity);

            //  width of outline
            $stroke_width = $svg->createAttribute("stroke-width");
            $stroke_width->value = $stroke;
            $circle->appendChild($stroke_width);

            //  no need to fill the space created by the size of this circle
            $fill = $svg->createAttribute("fill");
            $fill->value = "none";
            $circle->appendChild($fill);

            //  append the circle to the main svg
            $svg_root->appendChild($circle);
        }
    }

    //  Create elements to hold all the spawn data
    if (count($spawnList) > 0)
    {
        foreach ($spawnList as $spawnData)
        {
            //  begin transformation of the spawn angle
            $transform = 'translate('.($spawnData->x).','.($spawnData->y).')';
            $transform .= ' scale(1 -1) rotate(-90)';
            if (!is_null($spawnData->angle))
                $transform .= ' rotate('.($spawnData->angle).')';
            else
            {
                $l = sqrt($spawnData->xdir * $spawnData->xdir + $spawnData->ydir * $spawnData->ydir);
                if ($l == 0) echo $map_file;

                $transform .= ' matrix('.($spawnData->xdir / $l).','.($spawnData->ydir / $l).','.(-($spawnData->ydir) / $l).','.($spawnData->xdir / $l).',0,0)';
            }

            $path = $svg->createElement("path");

            $m_end= $svg->createAttribute("marker-end");
            $m_end->value = "url(#spawn)";
            $path->appendChild($m_end);

            $stroke_width = $svg->createAttribute("stroke-width");
            $stroke_width->value = "0.5";
            $path->appendChild($stroke_width);

            $stroke_t = $svg->createAttribute("stroke");
            $stroke_t->value = "red";
            $path->appendChild($stroke_t);

            $d = $svg->createAttribute("d");
            $d->value = "M 0 0 0 1";
            $path->appendChild($d);

            $transform_t = $svg->createAttribute("transform");
            $transform_t->value = $transform;
            $path->appendChild($transform_t);

            $fill = $svg->createAttribute("fill");
            $fill->value = "none";
            $path->appendChild($fill);

            $svg_root->appendChild($path);
        }
    }

    //  finish and output to file!
    $svg->appendChild($svg_root);
    $svg->save($fileSave);
}

//  the beginning of the svg file to start building
function buildSvg(DOMDocument &$svg, DOMElement &$svg_root, $view_line, $width, $height)
{
    $svg_root = $svg->createElement('svg');

    $xmlns = $svg->createAttribute("xmlns");
    $xmlns->value = "http://www.w3.org/2000/svg";
    $svg_root->appendChild($xmlns);

    //  viewbox
    $viewbox = $svg->createAttribute("viewBox");
    $viewbox->value = $view_line;
    $svg_root->appendChild($viewbox);

    //  width and height
    $width_t  = $svg->createAttribute("width");
    $height_t = $svg->createAttribute("height");
    $width_t->value  = $width;
    $height_t->value = $height;
    $svg_root->appendChild($width_t);
    $svg_root->appendChild($height_t);

    $defs = $svg->createElement("defs");
    $marker = $svg->createElement("marker");

    $id = $svg->createAttribute("id");
    $id->value = "spawn";
    $marker->appendChild($id);

    $m_width = $svg->createAttribute("markerWidth");
    $m_width->value = "30";
    $marker->appendChild($m_width);

    $m_height = $svg->createAttribute("markerHeight");
    $m_height->value = "40";
    $marker->appendChild($m_height);

    $refX = $svg->createAttribute("refX");
    $refX->value = "2";
    $marker->appendChild($refX);

    $refY = $svg->createAttribute("refY");
    $refY->value = "15";
    $marker->appendChild($refY);

    $orient = $svg->createAttribute("orient");
    $orient->value = "auto";
    $marker->appendChild($orient);

    $viewbox = $svg->createAttribute("viewBox");
    $viewbox->value = "0 0 40 30";
    $marker->appendChild($viewbox);

    $path = $svg->createElement("path");

    $d = $svg->createAttribute("d");
    $d->value = "M -5,0 -5,20 -15,20 0,40 15,20 5,20 5,0 Z";
    $path->appendChild($d);

    $fill = $svg->createAttribute("fill");
    $fill->value = "magenta";
    $path->appendChild($fill);

    $transform = $svg->createAttribute("transform");
    $transform->value = "translate(0,15) rotate(-90)";
    $path->appendChild($transform);

    $marker->appendChild($path);
    $defs->appendChild($marker);
    $svg_root->appendChild($defs);
}

//  function used to get the size mutiplier by the given $i = size factor
function exponent($i)
{
    $abs = $i;
    if ( $abs < 0 )
        $abs = -$abs;

    $ret = 1;
    $fac = sqrt(2);

    while ($abs > 0)
    {
        if ( 1 == ($abs & 1) )
            $ret *= $fac;

        $fac *= $fac;
        $abs >>= 1;
    }

    if ($i < 0)
        $ret = 1/$ret;

    return $ret;
}