<?php

//  obtain the overall rank of the player by this $player_hash
function getOverallRank($player_hash, $server_type = 0, $year = "NOW")
{
    if ((Filter($year) == "now") || ($year == 0))
        $year = date("Y");

    if ($server_type > 0)
        $maps_result = Data::$data->sql->query('SELECT * FROM `maps` WHERE `server_number`="'.$server_type.'"');
    else
        $maps_result = Data::$data->sql->query('SELECT * FROM `maps`');

    $maps = $maps_result->num_rows;

    $records_result = Data::$data->sql->query('SELECT * FROM `records` WHERE `year`="'.$year.'"');
    if ($records_result->num_rows > 0)
    {
        while ($row = $records_result->fetch_assoc())
        {
            if ($server_type > 0)
            {
                $maps_result = Data::$data->sql->query('SELECT * FROM `maps` WHERE `map_id`="'.$row['map_id'].'" AND `server_number`="'.$server_type.'"');
                if ($maps_result->num_rows == 0) continue;
            }

            $player_result = Data::$data->sql->query('SELECT * FROM `players` WHERE `player_id`="'.$row['player_id'].'"');
            $player_proces = $player_result->fetch_assoc();

            $record = null;
            if (!Record::checkPlayerById($player_proces['player_id']))
            {
                $record = new Record($player_proces['player_id'], $player_proces['player_name'], $player_proces['player_hash']);
            }
            else
            {
                $record = Record::getPlayerById($player_proces['player_id']);
            }

            if (isset($record) && !is_null($record) && $record instanceof Record)
            {
                if ($row['ranks'] == 1)  $record->rank_1++;
                if ($row['ranks'] == 2)  $record->rank_2++;
                if ($row['ranks'] == 3)  $record->rank_3++;
                if ($row['ranks'] <= 10) $record->top_ten++;

                $record->ranker += $row['ranks'];
                $record->maps   ++;
                $record->played += $row['finished'];
            }
        }
    }

    if (count(Record::$records) > 0)
    {
        assignRankingScores($maps);
        usort(Record::$records, 'sortRankingsRank');
        sortRankingsRanks();

        foreach (Record::$records as $record)
        {
            if (isset($record) && !is_null($record) && $record instanceof Record)
            {
                if ($record->hash == $player_hash)
                {
                    return $record->rank;
                }
            }
        }
    }
}

//  assign the ranking scores to players
function assignRankingScores($maps)
{
    if (count(Record::$records) > 0)
    {
        foreach (Record::$records as $record)
        {
            if (isset($record) && !is_null($record) && $record instanceof Record)
            {
                if ($record->played == 0)
                    $record->played = 1;

                $record->score = round((($record->ranker / $record->maps) *$maps) / $record->played, 4);
            }
        }
    }
}

//  sort the ranks by their score
function sortRankingsRank(Record $a, Record $b)
{
    return ($a->score > $b->score);
}

//  sort the rankings rank: lowest to highest
function sortRankingsRanks()
{
    $rank = 0;
    if (count(Record::$records) > 0)
    {
        foreach (Record::$records as $record)
        {
            $rank++;
            if (isset($record) && !is_null($record) && $record instanceof Record)
            {
                $record->rank = $rank;
            }
        }
    }
}

//  sort the rankings by their name: ascending
function sortRankingsAsc(Record $a, Record $b)
{
    return strcmp(strtolower($a->name), strtolower($b->name));
}

//  sort the rankings by their name: descending
function sortRankingsDesc(Record $a, Record $b)
{
    return strcmp(strtolower($b->name), strtolower($a->name));
}

//  view all the rankings of players
function viewRankings()
{
    loadHeaders();

    $view_year  = date("Y");
    if (isset(Data::$data->info[1]))
    {
        if (Filter(Data::$data->info[1]) == "year")
        {
            $view_year = intval(@Data::$data->info[2]);
            if ((Filter($view_year) == "now") || ($view_year == 0))
                $view_year = date("Y");
        }
    }

    $order_type = "RANK";
    if (isset(Data::$data->info[3]))
    {
        if (Filter(Data::$data->info[3]) == "ord")
            $order_type = @Data::$data->info[4];

        if ((Filter($order_type) != "asc") && (Filter($order_type) != "desc") && (Filter($order_type) != "rank"))
            $order_type = "RANK";
    }
    ?>
    <div class="title_header">
        <table>
            <tr>
                <td style="width: 85%;"><span class="title_header_txt">RANKINGS - <?=$view_year?></span></td>
                <td style="text-align: right;">
                    <form action="">
                        <select name="" onchange='return loadRankingsOrd(this, "<?php echo $view_year; ?>");'>
                            <option value="RANK" <?php if (Filter($order_type) == "rank") echo "selected"; ?>>Rank Order</option>
                            <option value="ASC" <?php if (Filter($order_type)  == "asc") echo "selected"; ?>>Ascending</option>
                            <option value="DESC" <?php if (Filter($order_type) == "desc") echo "selected"; ?>>Descending</option>
                        </select>
                        <select name="" onchange='return loadRankingsYear(this);'>
                        <select name="" onchange='return loadRankingsYear(this);'>
                            <option value="NOW" <?php if (Filter($view_year) == date("Y")) echo "selected"; ?>>Selected Year</option>
                            <?php
                            $records_result = Data::$data->sql->query('SELECT * FROM `backups` ORDER BY `backup_year` ASC');
                            if ($records_result->num_rows > 0)
                            {
                                while ($row = $records_result->fetch_assoc())
                                {
                                    ?>
                                    <option value="<?=$row['backup_year']?>" <?php if (Filter($view_year) == $row['backup_year']) echo "selected"; ?>><?=$row['backup_year']?></option>
                                <?php
                                }
                            }
                            ?>
                        </select>
                    </form>
                </td>
            </tr>
        </table>
    </div>
    <?php
    $maps_result = Data::$data->sql->query('SELECT * FROM `maps`');

    $maps = $maps_result->num_rows;

    $records_result = Data::$data->sql->query('SELECT * FROM `records`WHERE `year`="'.$view_year.'"');
    if ($records_result->num_rows > 0)
    {
        while ($row = $records_result->fetch_assoc())
        {
            $player_result = Data::$data->sql->query('SELECT * FROM `players` WHERE `player_id`="'.$row['player_id'].'"');
            $player_proces = $player_result->fetch_assoc();

            $record = null;
            if (!Record::checkPlayerById($player_proces['player_id']))
            {
                $record = new Record($player_proces['player_id'], $player_proces['player_name'], $player_proces['player_hash']);
            }
            else
            {
                $record = Record::getPlayerById($player_proces['player_id']);
            }

            if (isset($record) && !is_null($record) && $record instanceof Record)
            {
                if ($row['ranks'] == 1)  $record->rank_1++;
                if ($row['ranks'] == 2)  $record->rank_2++;
                if ($row['ranks'] == 3)  $record->rank_3++;
                if ($row['ranks'] <= 10) $record->top_ten++;

                $record->ranker += $row['ranks'];
                $record->maps   ++;
                $record->played += $row['finished'];
            }
        }
    }

    if (count(Record::$records) > 0)
    {
        assignRankingScores($maps);
        usort(Record::$records, 'sortRankingsRank');
        sortRankingsRanks();

        if (Filter($order_type) == "asc")
            usort(Record::$records, 'sortRankingsAsc');
        elseif (Filter($order_type) == "desc")
            usort(Record::$records, 'sortRankingsDesc');
        ?>
        <div class="ranking_body">
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="tls_header tls_border" style="width: 2%;"><b>Rank</b></td>
                    <td class="tls_header tls_border" style="text-align: left;"><b>Player</b></td>
                    <td class="tls_header tls_border" style="width: 8%;"><b>1st</b></td>
                    <td class="tls_header tls_border" style="width: 8%;"><b>2nd</b></td>
                    <td class="tls_header tls_border" style="width: 8%;"><b>3rd</b></td>
                    <td class="tls_header tls_border" style="width: 8%;"><b>Top Ten</b></td>
                    <td class="tls_header tls_border" style="width: 8%;"><b>Finished</b></td>
                    <td class="tls_header tls_border" style="width: 8%;"><b>Score</b></td>
                </tr>
        <?php
        foreach (Record::$records as $record)
        {
            if (isset($record) && !is_null($record) && $record instanceof Record)
            {
                ?>
                <tr class="tls_select">
                    <td class="tls_row tls_border" style="color: #ff3300;"><?php echo $record->rank; ?></td>
                    <td class="tls_row tls_border record_selection" style="text-align: left;"><a class="record_link" href="<?=Data::$data->url."index.php/Player/".$record->hash?>"><?=html_entity_decode($record->name)?></a></td>
                    <td class="tls_row tls_border" style="color: #aa33aa;"><?php echo $record->rank_1; ?></td>
                    <td class="tls_row tls_border" style="color: #aa33aa;"><?php echo $record->rank_2; ?></td>
                    <td class="tls_row tls_border" style="color: #aa33aa;"><?php echo $record->rank_3; ?></td>
                    <td class="tls_row tls_border" style="color: #aa33aa;"><?php echo $record->top_ten; ?></td>
                    <td class="tls_row tls_border" style="color: #aa33aa;"><?php echo $record->played; ?></td>
                    <td class="tls_row" style="color: #007700;"><?php echo $record->score; ?></td>
                </tr>
            <?php
            }
        }
        ?>
            </table>
        </div>
        <?php
    }
}

//  assign the scores of players by the highest amount
function assignHighScoresScores($maps)
{
    if (count(Record::$records) > 0)
    {
        foreach (Record::$records as $record)
        {
            if (isset($record) && !is_null($record) && $record instanceof Record)
            {
                if ($record->played == 0)
                    $record->played = 1;

                $record->score = round(($record->ranker / $record->maps) + $record->played);
            }
        }
    }
}

//  sort records of players by score: highest - lowest
function sortHighScoresRank(Record $a, Record $b)
{
    return ($a->score < $b->score);
}

//  sort the ranks of records by score: lowest - highest
function sortHighScoresRanks()
{
    $rank = 0;
    if (count(Record::$records) > 0)
    {
        foreach (Record::$records as $record)
        {
            $rank++;
            if (isset($record) && !is_null($record) && $record instanceof Record)
            {
                $record->rank = $rank;
            }
        }
    }
}

function viewHighScores()
{
    loadHeaders();

    $view_year  = date("Y");
    if (isset(Data::$data->info[1]))
    {
        if (Filter(Data::$data->info[1]) == "year")
        {
            $view_year = intval(@Data::$data->info[2]);
            if ((Filter($view_year) == "now") || ($view_year == 0))
                $view_year = date("Y");
        }
    }

    $order_type = "RANK";
    if (isset(Data::$data->info[3]))
    {
        if (Filter(Data::$data->info[3]) == "ord")
            $order_type = @Data::$data->info[4];

        if ((Filter($order_type) != "asc") && (Filter($order_type) != "desc") && (Filter($order_type) != "rank"))
            $order_type = "RANK";
    }
    ?>
    <div class="title_header">
        <table>
            <tr>
                <td style=" width: 85%;"><span class="title_header_txt">HIGHSCORES - <?=$view_year?></span></td>
                <td style="text-align: right;">
                    <form action="">
                        <select name="" onchange='return loadHighScoresOrd(this, "<?php echo $view_year; ?>");'>
                            <option value="RANK" <?php if (Filter($order_type) == "rank") echo "selected"; ?>>Rank Order</option>
                            <option value="ASC" <?php if (Filter($order_type)  == "asc") echo "selected"; ?>>Ascending</option>
                            <option value="DESC" <?php if (Filter($order_type) == "desc") echo "selected"; ?>>Descending</option>
                        </select>
                        <select name="" onchange='return loadHighScoresYear(this);'>
                            <option value="NOW" <?php if (Filter($view_year) == date("Y")) echo "selected"; ?>>Selected Year</option>
                            <?php
                            $records_result = Data::$data->sql->query('SELECT * FROM `backups` ORDER BY `backup_year` ASC');
                            if ($records_result->num_rows > 0)
                            {
                                while ($row = $records_result->fetch_assoc())
                                {
                                    ?>
                                    <option value="<?=$row['backup_year']?>" <?php if (Filter($view_year) == $row['backup_year']) echo "selected"; ?>><?=$row['backup_year']?></option>
                                <?php
                                }
                            }
                            ?>
                        </select>
                    </form>
                </td>
            </tr>
        </table>
    </div>
    <?php
    $maps_result = Data::$data->sql->query('SELECT * FROM `maps`');
    $maps = $maps_result->num_rows;

    $records_result = Data::$data->sql->query('SELECT * FROM `records` WHERE `year`="'.$view_year.'"');
    if ($records_result->num_rows > 0)
    {
        while ($row = $records_result->fetch_assoc())
        {
            $player_result = Data::$data->sql->query('SELECT * FROM `players` WHERE `player_id`="'.$row['player_id'].'"');
            $player_proces = $player_result->fetch_assoc();

            $record = null;
            if (!Record::checkPlayerById($player_proces['player_id']))
                $record = new Record($player_proces['player_id'], $player_proces['player_name'], $player_proces['player_hash']);
            else
                $record = Record::getPlayerById($player_proces['player_id']);

            if (isset($record) && !is_null($record) && $record instanceof Record)
            {
                if ($row['ranks'] == 1)  $record->rank_1++;
                if ($row['ranks'] == 2)  $record->rank_2++;
                if ($row['ranks'] == 3)  $record->rank_3++;
                if ($row['ranks'] <= 10) $record->top_ten++;

                $record->ranker += $row['ranks'];
                $record->maps   ++;
                $record->played += $row['finished'];
            }
        }
    }

    if (count(Record::$records) > 0)
    {
        assignHighScoresScores($maps);
        usort(Record::$records, 'sortHighScoresRank');
        sortHighScoresRanks();

        if (Filter($order_type) == "asc")
            usort(Record::$records, 'sortRankingsAsc');
        elseif (Filter($order_type) == "desc")
            usort(Record::$records, 'sortRankingsDesc');
        ?>
        <div class="ranking_body">
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="tls_header tls_border" style="width: 2%;"><b>Rank</b></td>
                    <td class="tls_header tls_border" style="text-align: left;"><b>Player</b></td>
                    <td class="tls_header tls_border" style="width: 8%;"><b>1st</b></td>
                    <td class="tls_header tls_border" style="width: 8%;"><b>2nd</b></td>
                    <td class="tls_header tls_border" style="width: 8%;"><b>3rd</b></td>
                    <td class="tls_header tls_border" style="width: 8%;"><b>Top Ten</b></td>
                    <td class="tls_header tls_border" style="width: 8%;"><b>Finished</b></td>
                    <td class="tls_header tls_border" style="width: 8%;"><b>Score</b></td>
                </tr>
        <?php
        foreach (Record::$records as $record)
        {
            if (isset($record) && !is_null($record) && $record instanceof Record)
            {
                ?>
                <tr class="tls_select">
                    <td class="tls_row tls_border" style="color: #ff3300;"><?php echo $record->rank; ?></td>
                    <td class="tls_row tls_border record_selection" style="text-align: left;"><a class="record_link" href="<?=Data::$data->url."index.php/Player/".$record->hash?>"><?=html_entity_decode($record->name)?></a></td>
                    <td class="tls_row tls_border" style="color: #aa33aa;"><?php echo $record->rank_1; ?></td>
                    <td class="tls_row tls_border" style="color: #aa33aa;"><?php echo $record->rank_2; ?></td>
                    <td class="tls_row tls_border" style="color: #aa33aa;"><?php echo $record->rank_3; ?></td>
                    <td class="tls_row tls_border" style="color: #aa33aa;"><?php echo $record->top_ten; ?></td>
                    <td class="tls_row tls_border" style="color: #aa33aa;"><?php echo $record->played; ?></td>
                    <td class="tls_row" style="color: #007700;"><?php echo $record->score; ?></td>
                </tr>
            <?php
            }
        }
        ?>
            </table>
        </div>
        <?php
    }
}
?>