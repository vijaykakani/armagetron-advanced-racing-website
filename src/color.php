<?php

//  fetch the colored string
function getColoredString($text, $font_size)
{
    $text = str_replace("\n", "\n0xRESETT", $text);

    $matches = array();
    $line = "";

    $text = str_replace("0xRESETT", "0xf9f9f9", $text);

    $strings = preg_split('/0x....../', $text);
    preg_match_all('/0x....../', $text, $matches);

    /*
    print_r($matches);
    print_r($strings);
    */

    foreach ($strings as $key => $string)
    {
        $color_code = @$matches[0][$key - 1];
        if ($color_code == '')
            $color_code = '0xf9f9f9';

        $r_hex = hexdec($color_code[2]) *16 + hexdec($color_code[3]);
        $g_hex = hexdec($color_code[4]) *16 + hexdec($color_code[5]);
        $b_hex = hexdec($color_code[6]) *16 + hexdec($color_code[7]);

        $r = CTR($r_hex);
        $g = CTR($g_hex);
        $b = CTR($b_hex);

        if (isDark($r, $g, $b))
            $color_code .= "; background: #f9f9f9";

        $line .= '<span style="color: ' . str_replace("0x", "#", strtolower($color_code)). ';">' . $string . '</span>';
    }

    return $line;
}

function CTR($x)
{
    return ($x / 255.0);
}

function extractColorCode($str, &$pos)
{
    //  increment twice to avoid 0x
    $colorcode = substr($str, $pos + 2, 6);
    $pos += 8;

    if ($colorcode == "RESETT")
        $colorcode = "ffffff";

    $r_hex = hexdec($colorcode[0]) *16 + hexdec($colorcode[1]);
    $g_hex = hexdec($colorcode[2]) *16 + hexdec($colorcode[3]);
    $b_hex = hexdec($colorcode[4]) *16 + hexdec($colorcode[5]);

    $r = CTR($r_hex);
    $g = CTR($g_hex);
    $b = CTR($b_hex);

    if (isDark($r, $g, $b))
        $colorcode .= "; background: #f9f9f9";

    return $colorcode;
}

function isDark($r, $g, $b)
{
    $minR = $minB = $minG = 0.5;
    $minTotal = 0.7;

    return (( $r < $minR && $g < $minG && $b < $minB ) || $r+$g+$b < $minTotal);
}