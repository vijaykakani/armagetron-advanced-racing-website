<?php

function downloadChatLogs()
{
    if (!Data::$data->ftp->connected) return;

    $root_path = "/home/vertrex/normal/servers";
    Data::$data->ftp->download(Data::$data->logs."area1/var/chatlog_colors.txt", $root_path."/racing_area1/var/chatlog_colors.txt", FTP_ASCII);
}

function viewChatLog()
{
    downloadChatLogs();

    loadHeaders();
    ?>
    <div class="title_header"><span class="title_header_txt">CHAT LOG - AREA 1</span> <img title="News" style="float: right;" src="<?php echo Data::$data->url; ?>/images/icons/chat.png" /></div>
    <table style="height: 500px; width: 100%; border: 1px solid #AAAAAA;">
        <tr>
            <td style="vertical-align: top; word-break: break-all; background: #000000;">
                <div style="font-size:16px; color: #f9f9f9; overflow: auto; height: 100%; width: 100%; font-family: 'Armagetronad';">
                    <?php
                    $content = @file_get_contents(Data::$data->logs."area1/var/chatlog_colors.txt");
                    if (Filter($content) != "")
                    {
                        $chat_messages = explode("\n", $content);
                        foreach ($chat_messages as $message)
                        {
                            if (trim($message) == "") continue;

                            $pos = strpos($message, "] ");
                            if ($pos !== false)
                            {
                                $date = substr($message, 0, $pos);
                                $string = substr($message, $pos + 1);

                                echo getColoredString($string, 16)."<br />\n";
                            }
                        }
                    }
                    else
                    {
                        ?>
                        <div class="player_body" style="padding: 10px; font-weight: bold;">No chat to display...</div>
                    <?php
                    }
                    ?>
                </div>
            </td>
        </tr>
    </table>
    <?php
    /*
    <br />
    <div class="title_header"><span class="title_header_txt">CHAT LOG - AREA 2</span> <img title="News" style="float: right;" src="<?php echo Data::$data->url; ?>/images/icons/chat.png" /></div>
    <table style="height: 500px; width: 100%; border: 1px solid #AAAAAA;">
        <tr>
            <td style="vertical-align: top; word-break: break-all; background: #000000; padding-left: 10px;">
                <div style="font-size:16px; color: #f9f9f9; overflow: auto; height: 100%; width: 100%; font-family: 'Armagetronad';">
                    <?php
                    $content = file_get_contents(@Data::$data->logs."area2/var/chatlog_colors.txt");
                    if (Filter($content) != "")
                    {
                        $chat_messages = explode("\n", $content);
                        foreach ($chat_messages as $message)
                        {
                            if (trim(sanitize($message)) == "") continue;

                            $pos = 0;
                            $f = extractNonCharString($message, "]", $pos);
                            if ($f != "")
                            {
                                $date   = substr($message, 0, $pos + 2);
                               $string = substr($message, $pos + 1);

                                echo getColoredString($string, 16)."<br />\n";
                            }
                        }
                    }
                    else
                    {
                        ?>
                        <div class="player_body" style="padding: 10px; font-weight: bold;">No chat to display...</div>
                    <?php
                    }
                    ?>
                </div>
            </td>
        </tr>
    </table>
<?php
    */
}
?>