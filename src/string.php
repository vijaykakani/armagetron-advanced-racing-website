<?php


// *******************************************************************************************
// *
// *   endswith
// *
// *******************************************************************************************
//!
//!        @param      $search                       The string to search in
//!        @param      $find                         The string to search for at the end
//!        @param      $sensitive: true or false     Should the search be sensitive or not
//!        @return                                   true if $search ends with $find
//!
// *******************************************************************************************
function endswith($search, $find, $sensitive = true)
{
    if (strlen($find) > strlen($search))
        return false;

    if (!$sensitive)
    {
        $isThis = strtolower(substr($search, strlen($search) - strlen($find)));
        if ($isThis == strtolower($find))
            return true;
    }
    else
    {
        $isThis = substr($search, strlen($search) - strlen($find));
        if ($isThis == $find)
            return true;
    }

    return false;
}

// *******************************************************************************************
// *
// *   startswith
// *
// *******************************************************************************************
//!
//!        @param      $search                       The string to search in
//!        @param      $find                         The string to search for at the beginning
//!        @param      $sensitive: true or false     Should the search be sensitive or not
//!        @return                                   true if $search starts with $find
//!
// *******************************************************************************************
function startswith($search, $find, $sensitive = true)
{
    if (strlen($find) > strlen($search))
        return false;

    if (!$sensitive)
    {
        $isThis = strtolower(substr($search, 0, strlen($find)));
        if ($isThis == strtolower($find))
            return true;
    }
    else
    {
        $isThis = substr($search, 0, strlen($find));
        if ($isThis == $find)
            return true;
    }

    return false;
}

// **********************************************************************
// *
// *	contains
// *
// **********************************************************************
//!
//!    @param      $search                       The string to search in
//!    @param      $find                         The string to search for
//!    @param      $sensitive: true or false     Should the search be sensitive or not
//!    @return     true                          Returns true if $find is found
//!
// **********************************************************************
function contains($search, $find, $sensitive = true)
{
    if (strlen($find) > strlen($search))
        return false;

    for($i = 0; $i < strlen($search); $i++)
    {
        if (!$sensitive)
        {
            $isThis = strtolower(substr($search, $i, strlen($find)));
            if ($isThis == strtolower($find))
                return true;
        }
        else
        {
            $isThis = substr($search, $i, strlen($find));
            if ($isThis == $find)
                return true;
        }
    }

    return false;
}

//!<  extracts substring without spaces from that position
function isBlank($str)
{
    if (is_null($str)) return true;

    if ($str == "") return true;

    if ($str == " ") return true;

    return false;
}

// *******************************************************************************************
// *
// *    extractNonBlankString
// *
// *******************************************************************************************
//!
//!    @param   $str             The string to be extracting from
//!    @param   $pos             Position where to start the extraction.
//!                              will contain the first position after the extracted string.
//!    @return                   The substring extracted. Leading blanks will not be taken.
//!
// *******************************************************************************************
function extractNonBlankString($str, &$pos)
{
    if ($pos > strlen($str))
        return "";

    $toReturn = "";

    while (($pos < strlen($str)) && isBlank($str[$pos]))
    {
        $pos++;
    }

    if ($pos > strlen($str))
        return "";

    while (($pos < strlen($str)) && !isBlank($str[$pos]))
    {
        $toReturn .= $str[$pos++];
    }

    if ($pos > strlen($str))
        return "";

    return $toReturn;
}

function extractNonCharString($str, $char, &$pos)
{
    if ($pos > strlen($str))
        return "";

    $toReturn = "";

    while (($pos < strlen($str)) && ($str[$pos] == $char[0]))
    {
        $pos++;
    }

    if ($pos > strlen($str))
        return "";

    while (($pos < strlen($str)) && ($str[$pos] != $char[0]))
    {
        $toReturn .= $str[$pos++];
    }

    if ($pos > strlen($str))
        return "";

    return $toReturn;
}

//!< Adding spaces till the designated position
function setpos(&$str, $pos)
{
    for($i = strlen($str); $i < $pos; $i++)
    {
        $str .= " ";
    }
}

//! @returns filtered string and with all of its all characters in lowercased string
function Filter($str, $lowercase = true, $unknown = '_')
{
    $filter = array();

    // map all unknown characters to underscores
    for ($i = 255; $i > 0; $i--)
    {
        $filter[$i] = $unknown;
    }

    // leave ASCII characters as they are
    for ($i = 126; $i > 32; $i--)
    {
        $filter[$i] = chr($i);
    }

    // ok, some of those are a bit questionable, but still better than _...
    $filter[(161)] = '!';
    $filter[(162)] = 'c';
    $filter[(163)] = 'l';
    $filter[(165)] = 'y';
    $filter[(166)] = '|';
    $filter[(167)] = 's';
    $filter[(168)] = '"';
    $filter[(169)] = 'c';
    $filter[(170)] = 'a';
    $filter[(171)] = '"';
    $filter[(172)] = '!';
    $filter[(174)] = 'r';
    $filter[(176)] = 'o';
    $filter[(177)] = '+';
    $filter[(178)] = '2';
    $filter[(179)] = '3';
    $filter[(182)] = 'p';
    $filter[(183)] = '.';
    $filter[(185)] = '1';
    $filter[(187)] = '"';
    $filter[(198)] = 'a';
    $filter[(199)] = 'c';
    $filter[(208)] = 'd';
    $filter[(209)] = 'n';
    $filter[(215)] = 'x';
    $filter[(216)] = 'o';
    $filter[(221)] = 'y';
    $filter[(222)] = 'p';
    $filter[(231)] = 'c';
    $filter[(241)] = 'n';
    $filter[(247)] = '/';
    $filter[(248)] = 'o';
    $filter[(253)] = 'y';
    $filter[(254)] = 'p';
    $filter[(255)] = 'y';

    //map 0 to o because they look similar
    $filter[(000)] = 'o';

    //  eat whitespace from left and right ends of string
    $str = trim($str);

    // filter out illegal characters
    for($i = 0; $i < strlen($str); $i++)
    {
        if (isset($filter[ord($str[$i])]))
        {
            $str[$i] = $filter[ord($str[$i])];
        }
    }

    if ($lowercase)
    {
        //  convert string to lower case
        $str = strtolower($str);
    }

    return $str;
}

//  use this function to sanitize input
function sanitize($str)
{
    //  null check
    if (strpos($str, chr(0)))
    {
        die("$str is not permitted as input.");
    }

    //  newline check
    if (strpos($str, chr(10)) || (strpos($str, chr(13))))
    {
        die("$str is not permitted as input.");
    }

    $meta = array('$', '{', '}', '[', ']', '`', ';');
    $escaped = array('&#36', '&#123', '&#125', '&#91', '&#93', '&#96', '&#59' );

    //  addslashes for quotes and backslashes
    $out = addslashes($str);

    //  str_replace for php meta characters
    $out = str_replace($meta, $escaped, $out);

    return Filter(Data::$data->sql->real_escape_string($out), false);
}