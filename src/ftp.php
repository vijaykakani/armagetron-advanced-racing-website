<?php
class Ftp
{
    var $ftp = null;

    var $host = "";
    var $port = 21;

    var $connected = false;

    function __construct($host, $port = 21)
    {
        $this->host = $host;
        $this->port = $port;

        $this->ftp = @ftp_connect($this->host, $this->port);
    }

    function login($username, $password)
    {
        if ($this->ftp)
            $this->connected = ftp_login($this->ftp, $username, $password);
    }

    function download($local_path, $remote_path, $mode = FTP_ASCII)
    {
        ftp_get($this->ftp, $local_path, $remote_path, $mode);
    }

    function dir_list($directory)
    {
        return ftp_nlist($this->ftp, $directory);
    }

    function close()
    {
        if ($this->ftp)
            ftp_close($this->ftp);
    }
};