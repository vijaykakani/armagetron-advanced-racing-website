<?php

function loadHome()
{
    loadHeaders();
    ?>
    <table>
        <tr>
            <td style="vertical-align: top; padding-right: 10px; width: 30%;">
                <?php displayOnlinePlayers(); ?>
                <br />
                <a href="http://grooveshark.com/#!/alphaproject/broadcast"><div class="title_header" style="color: #444444; font-size: 18px; font-weight: bold;">Music Stream <img title="Server" style="float: right;" src="<?php echo Data::$data->url; ?>/images/icons/music.png" /></div></a>
            </td>
            <td style="vertical-align: top; border: 1px solid #AAAAAA;"><?php displayLimitedNews(3); ?></td>
        </tr>
    </table>
    <?php
}

function WrongPage()
{
    loadHeaders();
    ?>
    <div style="background: #f9f9f9; font-size: 90px; text-align: center;">
        404 - UNKNOWN!
    </div>
<?php
}
?>