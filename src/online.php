<?php

function downloadFiles()
{
    if (!Data::$data->ftp->connected) return;

    $root_path = "/home/vertrex/normal/servers";

    Data::$data->ftp->download(Data::$data->logs."area1/console.txt", $root_path."/racing_area1/console.txt", FTP_ASCII);
    Data::$data->ftp->download(Data::$data->logs."area2/console.txt", $root_path."/racing_area2/console.txt", FTP_ASCII);
}

function downloadOnlinePlayersLog()
{
    if (!Data::$data->ftp->connected) return;

    $root_path = "/home/vertrex/normal/servers";

    if (!is_dir(Data::$data->logs."area1/var"))
        mkdir(Data::$data->logs."area1/var");

    Data::$data->ftp->download(Data::$data->logs."area1/var/online_players.txt", $root_path."/racing_area1/var/online_players.txt", FTP_ASCII);
}

function displayOnlinePlayers()
{
    downloadOnlinePlayersLog();

    ?>
    <div style="border: 1px solid #AAAAAA;">
        <div class="online_header">Online Players<img title="Server" style="float: right;" src="<?php echo Data::$data->url; ?>/images/icons/server.png" /></div>
        <?php
        $contents = explode("\n", trim(@file_get_contents(Data::$data->logs."area1/var/online_players.txt")));
        if ((Filter($contents[0]) == "nobody_online.") || (Filter($contents[0]) == ""))
        {
            if (Filter($contents[0]) == "")
                $contents[0] = "Server Offline.";
            ?><div class="online_stats"><?php echo $contents[0];?></div><?php
        }
        else
        {
            ?>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                    <?php
                    $map_file = $contents[0];
                    for($i = 1; $i < count($contents); $i++)
                    {
                    ?>
                    <tr>
                        <?php
                        $player_1 = $contents[$i];
                        $player_2 = "";
                        $i++;
                        if (isset($contents[$i]))
                            $player_2 = $contents[$i];

                        $pos = 0;
                        $login = htmlentities(extractNonBlankString($player_1, $pos));
                        $alive = extractNonBlankString($player_1, $pos);
                        $ping  = round(extractNonBlankString($player_1, $pos));
                        $score = extractNonBlankString($player_1, $pos);
                        $a_lvl = extractNonBlankString($player_1, $pos);
                        $name  = htmlentities(substr($player_1, $pos + 1));

                        if ($name == '')
                            $name = $login;
                        ?>
                        <td class="online_player" style="border-top: 1px solid #AAAAAA;"><label <?php echo 'title="Username: '.$login."\nPing: ".$ping."\nScore: ".$score.'">'?><?php echo $name; ?></label></td>
                        <?php
                        if (Filter($player_2) != "")
                        {
                            $pos = 0;
                            $login = htmlentities(extractNonBlankString($player_2, $pos));
                            $alive = extractNonBlankString($player_2, $pos);
                            $ping  = round(extractNonBlankString($player_2, $pos));
                            $score = extractNonBlankString($player_2, $pos);
                            $a_lvl = extractNonBlankString($player_2, $pos);
                            $name  = htmlentities(substr($player_2, $pos + 1));

                            if ($name == '')
                                $name = $login;
                            ?>
                            <td class="online_player" style="border-left: 1px solid #AAAAAA; border-top: 1px solid #AAAAAA;"><label <?php echo 'title="Username: '.$login."\nPing: ".$ping."\nScore: ".$score.'">'?><?php echo $name; ?></td>
                            <?php
                        }
                        ?>
                    </tr>
                    <?php
                    }
                    ?>
            </table>
        <?php
        }
        /*
        ?>
    </div>
    ?>
    <br />
    <div style="border: 1px solid #AAAAAA;">
        <div class="online_header">Area 2 | New Maps <img title="Server" style="float: right;" src="<?php echo Data::$data->url; ?>/images/icons/server.png" /></div>
        <?php
        $contents = explode("\n", trim(file_get_contents(Data::$data->logs."area2/var/online_players.txt")));
        if ((Filter($contents[0]) == "nobody_online.") || (Filter($contents[0]) == ""))
        {
            if (Filter($contents[0]) == "")
                $contents[0] = "Nobody Online.";
            ?><div class="online_stats"><?php echo $contents[0];?></div><?php
        }
        else
        {
            ?>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <?php
                $map_file = $contents[0];
                for($i = 1; $i < count($contents); $i++)
                {
                    ?>
                    <tr>
                        <?php
                        $player_1 = $contents[$i];
                        $player_2 = "";
                        $i++;
                        if (isset($contents[$i]))
                            $player_2 = $contents[$i];

                        $pos = 0;
                        $login = extractNonBlankString($player_1, $pos);
                        $alive = extractNonBlankString($player_1, $pos);
                        $ping  = extractNonBlankString($player_1, $pos);
                        $score = extractNonBlankString($player_1, $pos);
                        $a_lvl = extractNonBlankString($player_1, $pos);
                        $name  = substr($player_1, $pos + 1);
                        ?>
                        <td class="online_player" style="border-top: 1px solid #AAAAAA;"><label <?php echo 'title="Username: '.$login."\nPing: ".$ping."\nScore: ".$score.'">'?><?php echo $name; ?></label></td>
                        <?php
                        if (Filter($player_2) != "")
                        {
                            $pos = 0;
                            $login = extractNonBlankString($player_2, $pos);
                            $alive = extractNonBlankString($player_2, $pos);
                            $ping  = extractNonBlankString($player_2, $pos);
                            $score = extractNonBlankString($player_2, $pos);
                            $a_lvl = extractNonBlankString($player_2, $pos);
                            $name  = substr($player_2, $pos + 1);
                            ?>
                            <td class="online_player" style="border-left: 1px solid #AAAAAA; border-top: 1px solid #AAAAAA;"><label <?php echo 'title="Username: '.$login."\nPing: ".$ping."\nScore: ".$score.'">'?><?php echo $name; ?></td>
                        <?php
                        }
                        ?>
                    </tr>
                <?php
                }
                ?>
            </table>
        <?php
        }
    */
        ?>
    </div>
    <?php
}