<?php

class Record
{
    static $records = array();

    var $id   = 0;
    var $name = "";
    var $hash = "";

    var $rank = 0;
    var $time = 0;

    var $rank_1  = 0;
    var $rank_2  = 0;
    var $rank_3  = 0;
    var $top_ten = 0;
    var $played  = 0;
    var $score   = 0;

    var $maps = 0;
    var $ranker = 0;

    var $maps_played = array();

    function __construct($id, $name, $hash, $add = true)
    {
        $this->id   = $id;
        $this->name = $name;
        $this->hash = $hash;

        if ($add)
            self::$records[] = $this;
    }

    static function checkPlayerById($id)
    {
        if (count(self::$records) > 0)
        {
            foreach (self::$records as $record)
            {
                if (isset($record) && !is_null($record) && $record instanceof Record && ($record->id == $id))
                    return true;
            }
        }

        return false;
    }

    static function getPlayerById($id)
    {
        if (count(self::$records) > 0)
        {
            foreach (self::$records as $record)
            {
                if (isset($record) && !is_null($record) && $record instanceof Record && ($record->id == $id))
                    return $record;
            }
        }

        return null;
    }
}

//  list all players who have played in the maps
function listPlayers()
{
    loadHeaders();

    $order_type = "ORDER";
    if (isset(Data::$data->info[1]))
    {
        if (Filter(Data::$data->info[1]) == "ord")
            $order_type = @Data::$data->info[2];

        if ((Filter($order_type) != "asc") && (Filter($order_type) != "desc") && (Filter($order_type) != "order"))
            $order_type = "ORDER";
    }
    ?>
    <div class="title_header">
        <table>
            <tr>
                <td style="width: 34%;"><span class="title_header_txt">PLAYERS</span></td>
                <td style="text-align: right; width: 65%;"><input type="text" id="filter" style="height:40px;font-size:20pt;width:100%;" placeholder="Type to search.."></td>
                <td style="text-align: right;">
                    <form action="">
                        <select name="" onchange='return loadPlayersOrd(this);'>
                            <option value="ORDER" <?php if (Filter($order_type) == "order") echo "selected"; ?>>Player Order</option>
                            <option value="ASC" <?php if (Filter($order_type)  == "asc") echo "selected"; ?>>Ascending</option>
                            <option value="DESC" <?php if (Filter($order_type) == "desc") echo "selected"; ?>>Descending</option>
                        </select>
                    </form>
                </td>
            </tr>
        </table>
    </div>
    <div class="player_body">
        <table cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td class="tls_header tls_border" style="text-align: left;"><b>Player</b></td>
                <td class="tls_header tls_border" style="width: 8%;"><b>Hash</b></td>
            </tr>
            <?php
            if (Filter($order_type) != "order")
                $order_type = " ORDER BY `player_name` ".$order_type;
            else
                $order_type = "";

            $players_result = Data::$data->sql->query('SELECT * FROM `players`'.$order_type);
            while ($player = $players_result->fetch_assoc())
            {
            ?>
            <tr class="tls_select">
                <td  class="tls_row tls_border record_selection" style="text-align: left;"><a class="record_link" href="<?=Data::$data->url."index.php/Player/".$player['player_hash']?>"><?=html_entity_decode($player['player_name'])?></a></td>
                <td  class="tls_row" style="color: #aa33aa; padding: 10px;"><?php echo $player['player_hash']; ?></td>
            </tr>
            <?php
            }
            ?>
        </table>
    </div>
    <?php
}

//  sort the players by their rank: lowest to highest
function sortPlayerMapsRank(Map $a, Map $b)
{
    return ($a->rank > $b->rank);
}

//  sort the players by their name: ascending
function sortPlayerMapsAsc(Map $a, Map $b)
{
    return strcmp(strtolower($a->name), strtolower($b->name));
}

//  sort the players by their name: descending
function sortPlayerMapsDesc(Map $a, Map $b)
{
    return strcmp(strtolower($b->name), strtolower($a->name));
}

//  view all the records of players
function viewPlayerRecords()
{
    $server_type = 0;
    /*
    if (isset(Data::$data->info[2]))
    {
        if (Filter(Data::$data->info[2]) == "srv")
            $server_type = @intval(Data::$data->info[3]);

        if (($server_type < 0) || ($server_type > 2))
            $server_type = 0;
    }
    */

    $view_year  = date("Y");
    if (isset(Data::$data->info[2]))
    {
        if (Filter(Data::$data->info[2]) == "year")
        {
            $view_year = intval(@Data::$data->info[3]);
            if ((Filter($view_year) == "now") || ($view_year == 0))
                $view_year = date("Y");
        }
    }

    $order_type = "RANK";

    if (isset(Data::$data->info[4]))
    {
        if (Filter(Data::$data->info[4]) == "ord")
        {
            $order_type = @Data::$data->info[5];
            if ((Filter($order_type) != "asc") && (Filter($order_type) != "desc") && (Filter($order_type) != "rank"))
                $order_type = "RANK";
        }
    }

    $player_hash = "";
    if (isset(Data::$data->info[1]))
        $player_hash = Data::$data->info[1];

    $player_result = Data::$data->sql->query('SELECT * FROM `players` WHERE `player_hash`="'.$player_hash.'"');
    if ($player_result->num_rows == 1)
    {
        $player = $player_result->fetch_assoc();
        $record = new Record($player['player_id'], $player['player_name'], $player['player_hash'], false);
        $record->rank = getOverallRank($record->hash, $server_type, $view_year);

        loadHeaders($record->name);

        $ranker = 0;

        $records_result = Data::$data->sql->query('SELECT * FROM `records` WHERE `player_id`="'.$player['player_id'].'" AND `year`="'.$view_year.'"');
        if ($records_result->num_rows > 0)
        {
            while ($row = $records_result->fetch_assoc())
            {
                $map_result = null;
                if ($server_type > 0)
                {
                    $map_result = Data::$data->sql->query('SELECT * FROM `maps` WHERE `map_id`="'.$row['map_id'].'" AND `server_number`="'.$server_type.'"');
                    if ($map_result->num_rows == 0) continue;
                }
                else
                {
                    $map_result = Data::$data->sql->query('SELECT * FROM `maps` WHERE `map_id`="'.$row['map_id'].'"');
                }

                if (!is_null($map_result))
                {
                    $map_proces = $map_result->fetch_assoc();
                    $map = new Map($map_proces['map_id'], $map_proces['map_hash'], $map_proces['map_name'], $map_proces['map_author'], $map_proces['map_version'], $map_proces['map_category']);
                    $map->time = $row['times'];
                    $map->rank = $row['ranks'];
                    $map->finished = $row['finished'];

                    $record->maps_played[] = $map;
                }

                if ($row['ranks'] == 1)  $record->rank_1++;
                if ($row['ranks'] == 2)  $record->rank_2++;
                if ($row['ranks'] == 3)  $record->rank_3++;
                if ($row['ranks'] <= 10) $record->top_ten++;

                $ranker = $record->ranker += $row['ranks'];
                $record->played += $row['finished'];
                $record->maps++;
            }
        }

        ?>
        <div class="title_header" style="background: #ccccff;">
            <table>
                <tr>
                    <td colspan="2" style=" width: 100%; text-align: center;"><span class="title_header_txt">[ <?=$view_year?> ]</span></td>
                </tr>
                <tr>
                    <td style=" width: 100%;"><span class="title_header_txt"><?=html_entity_decode($player['player_name'])?></span></td>
                    <td style="text-align: right;">
                        <form action="">
                            <select name="" onchange='return loadPlayerOrd(this, "<?=$record->hash?>", "<?=$view_year?>");'>
                                <option value="RANK" <?php if (Filter($order_type) == "rank") echo "selected"; ?>>Map Order</option>
                                <option value="ASC" <?php if (Filter($order_type)  == "asc") echo "selected"; ?>>Ascending</option>
                                <option value="DESC" <?php if (Filter($order_type) == "desc") echo "selected"; ?>>Descending</option>
                            </select>
                            <select name="" onchange='return loadPlayerYear(this, "<?=$record->hash?>");'>
                                <option value="NOW" <?php if (Filter($view_year) == date("Y")) echo "selected"; ?>>Selected Year</option>
                                <?php
                                $records_result = Data::$data->sql->query('SELECT * FROM `backups` ORDER BY `backup_year` ASC');
                                if ($records_result->num_rows > 0)
                                {
                                    while ($row = $records_result->fetch_assoc())
                                    {
                                        ?>
                                        <option value="<?=$row['backup_year']?>" <?php if (Filter($view_year) == $row['backup_year']) echo "selected"; ?>><?=$row['backup_year']?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </form>
                    </td>
                </tr>
            </table>
        </div>

        <table cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="vertical-align: top; padding-right: 10px; width: 30%;">
                    <div class="player_body" style="padding: 10px;">
                        <?php
                        identicon(Data::$data->root."images/profiles/", $record->hash, 160);
                        ?>
                        <table>
                            <tr>
                                <td style="vertical-align: top; padding-right: 10px;"><img style="border: 2px outset #BBBBBB;" src="<?php echo Data::$data->url."images/profiles/".$record->hash.".png"; ?>" /></td>
                                <td style="vertical-align: top; width: 100%;">
                                    <table cellpadding="0" cellspacing="0" style="border: 1px outset #BBBBBB; display: block; width: 100%;">
                                        <tr>
                                            <td class="tls_border display_view"><b>Personal</b></td>
                                            <td class="display_view" style="color: #0000ff; text-align: center;"><?php if ($record->maps > 0) echo round($ranker/$record->maps); else echo 0; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="tls_border display_view"><b>Overall</b></td>
                                            <td class="display_view" style="color: #0000ff; text-align: center;"><?php echo $record->rank; ?></td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table cellpadding="0" cellspacing="0" style="border: 1px outset #BBBBBB; display: block; width: 100%;">
                                        <tr>
                                            <td class="tls_border display_view"><b>1st</b></td>
                                            <td class="display_view" style="color: #cc0055; text-align: center;"><?php echo $record->rank_1; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="tls_border display_view"><b>2nd</b></td>
                                            <td class="display_view" style="color: #cc0055; text-align: center;"><?php echo $record->rank_2; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="tls_border display_view"><b>3rd</b></td>
                                            <td class="display_view" style="color: #cc0055; text-align: center;"><?php echo $record->rank_3; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="tls_border display_view"><b>Top Ten</b></td>
                                            <td class="display_view" style="color: #cc0055; text-align: center;"><?php echo $record->top_ten; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="tls_border display_view"><b>Maps</b></td>
                                            <td class="display_view" style="color: #cc0055; text-align: center;"><?php echo $record->maps; ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td style="vertical-align: top;">
                    <div class="ranking_body">
                        <?php
                        if (count($record->maps_played) > 0)
                        {
                            usort($record->maps_played, 'sortPlayerMapsRank');

                            if (Filter($order_type) == "asc")
                                usort($record->maps_played, 'sortPlayerMapsAsc');
                            elseif (Filter($order_type) == "desc")
                                usort($record->maps_played, 'sortPlayerMapsDesc');
                        ?>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td class="tls_header tls_border" style="width: 2%;"><b>Rank</b></td>
                                <td class="tls_header tls_border" style="text-align: left;"><b>Map</b></td>
                                <td class="tls_header tls_border" style="width: 8%;"><b>Time (seconds)</b></td>
                                <td class="tls_header tls_border" style="width: 8%;"><b>Finished (crossed)</b></td>
                            </tr>
                        <?php
                            foreach ($record->maps_played as $map)
                            {
                                if (isset($map) && !is_null($map) && $map instanceof Map)
                                {
                                ?>
                                <tr class="tls_select">
                                    <td class="tls_row tls_border" style="color: #ff3300;"><?php echo $map->rank; ?></td>
                                    <td class="tls_row tls_border record_selection" style="text-align: left;"><a class="record_link" href="<?php echo Data::$data->url."index.php/Map/".$map->hash; ?>"><?php echo $map->name; ?></a></td>
                                    <td class="tls_row tls_border" style="color: #aa33aa; width: 10%;"><?php echo $map->time; ?></td>
                                    <td class="tls_row tls_border" style="color: #aa33aa;"><?php echo $map->finished; ?></td>
                                </tr>
                                <?php
                                }
                            }
                            ?>
                            </table>
                            <?php
                        }
                        else
                        {
                            ?>
                            <div class="ranking_body" style="padding: 10px; font-weight: bold;">No maps found...</div>
                        <?php
                        }
                        ?>
                    </div>
                </td>
            </tr>
        </table>
        <?php
    }
    else
    {
        loadHeaders();
        ?>
        <div class="player_body" style="padding: 10px; font-weight: bold;">Unknown player hash: "<?php echo $player_hash; ?>"</div>
        <?php
    }
}
?>