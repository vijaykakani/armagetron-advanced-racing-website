<?php

class Map
{
    var $id   = 0;
    var $hash = "";

    var $name = "";
    var $author = "";
    var $version = 0;
    var $category = "";
    var $map_file = "";

    var $time = 0;
    var $rank = 0;
    var $finished = 0;

    var $players = array();

    var $set = false;

    function __construct($id, $hash, $name, $author, $version, $category)
    {
        $this->id   = $id;
        $this->hash = $hash;

        $this->name = $name;
        $this->author = $author;
        $this->version = $version;
        $this->category = $category;
    }
};

//  list maps in their selected order or their respective server area
function listMaps()
{
    loadHeaders();

    $hd_msg = "";

    $order_type = "ORDER";
    if (isset(Data::$data->info[1]))
    {
        if (Filter(Data::$data->info[1]) == "ord")
            $order_type = @Data::$data->info[2];

        if ((Filter($order_type) != "asc") && (Filter($order_type) != "desc") && (Filter($order_type) != "order"))
            $order_type = "ORDER";

        if (Filter($order_type) != "order")
            $hd_msg = $order_type;
    }

    $comp_type = "ALL";
    if (isset(Data::$data->info[1]))
    {
        if (Filter(Data::$data->info[1]) == "complete")
            $comp_type = @Data::$data->info[2];

        if ((Filter($comp_type) != "yes") && (Filter($comp_type) != "no") && (Filter($comp_type) != "all"))
            $comp_type = "ALL";

        if (Filter($comp_type) == "yes")
            $hd_msg = "COMPLETE";
        elseif (Filter($comp_type) == "no")
            $hd_msg = "INCOMPLETE";
    }

    ?>
    <div class="title_header">
        <table>
            <tr>
                <td style="width: 34%;"><span class="title_header_txt">MAPS <?php if (Filter($hd_msg) != "") echo " - ".$hd_msg; ?></span></td>
                <td style="text-align: right; width: 65%;"><input type="text" id="filter" style="height:40px;font-size:20pt;width:100%;" placeholder="Type to search.."></td>
                <td style="text-align: right;">
                    <form action="">
                        <select name="" onchange='return loadMapsOrd(this);'>
                            <option value="ORDER" <?php if (Filter($order_type) == "order") echo "selected"; ?>>Map Order</option>
                            <option value="ASC" <?php if (Filter($order_type)  == "asc") echo "selected"; ?>>Ascending</option>
                            <option value="DESC" <?php if (Filter($order_type) == "desc") echo "selected"; ?>>Descending</option>
                        </select>
                    </form>
                </td>
                <td style="text-align: right;">
                    <form action="">
                        <select name="" onchange='return loadMapsComp(this);'>
                            <option value="ALL" <?php if (Filter($comp_type) == "all") echo "selected"; ?>>Maps Done</option>
                            <option value="YES" <?php if (Filter($comp_type)  == "yes") echo "selected"; ?>>Complete</option>
                            <option value="NO" <?php if (Filter($comp_type) == "no") echo "selected"; ?>>Incomplete</option>
                        </select>
                    </form>
                </td>
            </tr>
        </table>
    </div>
    <div class="player_body">
        <table cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td class="tls_header tls_border" style="text-align: left;"><b>Map</b></td>
                <td class="tls_header tls_border" style="text-align: left;"><b>MAP_FILE</b></td>
                <td class="tls_header tls_border" style="width: 8%;"><b>Hash</b></td>
            </tr>
            <?php
            if (Filter($order_type) != "order")
                $order_type = " ORDER BY `map_name` ".$order_type;
            else
                $order_type = "";

            $maps_result = Data::$data->sql->query('SELECT * FROM `maps`'.$order_type);
            while ($map = $maps_result->fetch_assoc())
            {
                if (Filter($comp_type) != "all")
                {
                    if (Filter($comp_type) == "yes")
                        if ($map['map_exists'] == 0) continue;

                    if (Filter($comp_type) == "no")
                        if ($map['map_exists'] == 1) continue;
                }
                ?>
                <tr class="tls_select">
                    <td class="tls_row tls_border record_selection" style="text-align: left;"><a class="record_link" href="<?php echo Data::$data->url."index.php/Map/".$map['map_hash']; ?>"><?php echo $map['map_name']; ?></a></td>
                    <td class="tls_row tls_border record_selection" style="text-align: left; padding: 10px;"><?php echo $map['map_file']; ?></td>
                    <td class="tls_row tls_border record_selection" style="color: #aa33aa; padding: 10px;"><?php echo $map['map_hash']; ?></td>
                </tr>
            <?php
            }
            ?>
        </table>
    </div>
<?php
}

//  sort the maps by the players ranks (shortest - longest)
function sortMapsPlayerRank(Record $a, Record $b)
{
    return ($a->rank > $b->rank);
}

//  sort the maps by the players names: ascending
function sortMapsPlayerAsc(Record $a, Record $b)
{
    return strcmp(strtolower($a->name), strtolower($b->name));
}

//  sort the maps by the players names: descending
function sortMapsPlayerDesc(Record $a, Record $b)
{
    return strcmp(strtolower($b->name), strtolower($a->name));
}

//  view the map with players time records and times they finished it
function viewMapRecords()
{
    $server_type = 0;
    /*
    if (isset(Data::$data->info[2]))
    {
        if (Filter(Data::$data->info[2]) == "srv")
            $server_type = @intval(Data::$data->info[3]);

        if (($server_type < 0) || ($server_type > 2))
            $server_type = 0;
    }
    */

    $view_year  = date("Y");
    if (isset(Data::$data->info[2]))
    {
        if (Filter(Data::$data->info[2]) == "year")
        {
            $view_year = intval(@Data::$data->info[3]);
            if ((Filter($view_year) == "now") || ($view_year == 0))
                $view_year = date("Y");
        }
    }

    $order_type = "RANK";

    if (isset(Data::$data->info[4]))
    {
        if (Filter(Data::$data->info[4]) == "ord")
        {
            $order_type = @Data::$data->info[5];
            if ((Filter($order_type) != "asc") && (Filter($order_type) != "desc") && (Filter($order_type) != "rank"))
                $order_type = "RANK";
        }
    }

    $map_hash = "";
    if (isset(Data::$data->info[1]))
        $map_hash = Data::$data->info[1];

    $map_result = Data::$data->sql->query('SELECT * FROM `maps` WHERE `map_hash`="'.$map_hash.'"');// AND `map_exists`="1"');
    if ($map_result->num_rows == 1)
    {
        $map_t = $map_result->fetch_assoc();
        $map = new Map($map_t['map_id'], $map_t['map_hash'], $map_t['map_name'], $map_t['map_author'], $map_t['map_version'], $map_t['map_category']);
        $map->map_file = $map_t['map_file'];
        loadHeaders($map->name);

        if (isset(Data::$data->info[2]))
        {
            if (Filter(Data::$data->info[2]) == "preview")
            {
                ?>
                <a href="<?php echo Data::$data->url."index.php/Map/".$map->hash; ?>"><div class="title_header"><span class="title_header_txt"><?php echo $map->map_file; ?></span></div></a>
                <img style="border: 2px outset #BBBBBB; background-color: #333333;" src="<?php echo Data::$data->url."images/maps/".$map->hash.".svg"; ?>" />
                <?php
                return;
            }
        }

        $records_result = Data::$data->sql->query('SELECT * FROM `records` WHERE `map_id`="'.$map->id.'" AND `year`="'.$view_year.'"');
        if ($records_result->num_rows > 0)
        {
            while ($row = $records_result->fetch_assoc())
            {
                $player_result = Data::$data->sql->query('SELECT * FROM `players` WHERE `player_id`="'.$row['player_id'].'"');
                $player_proces = $player_result->fetch_assoc();
                $player = new Record($player_proces['player_id'], $player_proces['player_name'], $player_proces['player_hash']);

                $player->rank = $row['ranks'];
                $player->time = $row['times'];
                $map->finished += $player->played = $row['finished'];

                $map->players[] = $player;
            }
        }

        ?>
        <div class="title_header" style="background: #ccccff;">
            <table>
                <tr>
                    <td colspan="2" style=" width: 100%; text-align: center;"><span class="title_header_txt">[ <?=$view_year?> ]</span></td>
                </tr>
                <tr>
                    <td style=" width: 100%;"><span class="title_header_txt"><?=$map->name?></span></td>
                    <td style="text-align: right;">
                        <form action="">
                            <select name="" onchange='return loadMapOrd(this, "<?=$map->hash?>", "<?=$view_year?>");'>
                                <option value="RANK" <?php if (Filter($order_type) == "rank") echo "selected"; ?>>Rank Order</option>
                                <option value="ASC" <?php if (Filter($order_type)  == "asc") echo "selected"; ?>>Ascending</option>
                                <option value="DESC" <?php if (Filter($order_type) == "desc") echo "selected"; ?>>Descending</option>
                            </select>
                            <select name="" onchange='return loadMapYear(this, "<?=$map->hash?>");'>
                                <option value="NOW" <?php if (Filter($view_year) == date("Y")) echo "selected"; ?>>Selected Year</option>
                                <?php
                                $records_result = Data::$data->sql->query('SELECT * FROM `backups` ORDER BY `backup_year` ASC');
                                if ($records_result->num_rows > 0)
                                {
                                    while ($row = $records_result->fetch_assoc())
                                    {
                                        ?>
                                        <option value="<?=$row['backup_year']?>" <?php if (Filter($view_year) == $row['backup_year']) echo "selected"; ?>><?=$row['backup_year']?></option>
                                    <?php
                                    }
                                }
                                ?>
                            </select>
                        </form>
                    </td>
                </tr>
            </table>
        </div>

        <table cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="vertical-align: top; padding-right: 10px;">
                    <div class="player_body" style="padding: 10px; ">
                        <table style="width: 100%">
                            <tr>
                                <td style="vertical-align: top; width: 100%;">
                                    <a title="Click to enlarge"  href="<?php echo Data::$data->url."index.php/Map/".$map->hash; ?>/Preview"><img style="border: 2px outset #BBBBBB; background-color: #333333;" src="<?php echo Data::$data->url."images/maps/".$map->hash.".svg"; ?>" /></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">
                                    <table cellpadding="0" cellspacing="0" style="border: 1px outset #BBBBBB; display: block; width: 100%;">
                                        <tr>
                                            <td class="tls_border display_view"><b>Players</b></td>
                                            <td class="display_view" style="color: #0000ff; text-align: center;"><?php echo count($map->players);?></td>
                                        </tr>
                                        <tr>
                                            <td class="tls_border display_view"><b>Completed</b></td>
                                            <td class="display_view" style="color: #0000ff; text-align: center;"><?php echo $map->finished; ?></td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table cellpadding="0" cellspacing="0" style="border: 1px outset #BBBBBB; display: block; width: 100%;">
                                        <tr>
                                            <td class="tls_border display_view"><b>Name</b></td>
                                            <td class="display_view" style="color: #cc0055; text-align: center;"><?php echo $map->name; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="tls_border display_view"><b>Author</b></td>
                                            <td class="display_view" style="color: #cc0055; text-align: center;"><?php echo $map->author; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="tls_border display_view"><b>Category</b></td>
                                            <td class="display_view" style="color: #cc0055; text-align: center;"><?php echo $map->category; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="tls_border display_view"><b>Version</b></td>
                                            <td class="display_view" style="color: #cc0055; text-align: center;"><?php echo $map->version; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="display_view"><b><?php echo $map->map_file; ?></b></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td style="vertical-align: top; width: 65%;">
                    <div class="ranking_body">
                    <?php
                    if (count($map->players) > 0)
                    {
                        usort($map->players, 'sortMapsPlayerRank');

                        if (Filter($order_type) == "asc")
                            usort($map->players, 'sortMapsPlayerAsc');
                        elseif (Filter($order_type) == "desc")
                            usort($map->players, 'sortMapsPlayerDesc');
                        ?>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td class="tls_header tls_border" style="width: 2%;"><b>Rank</b></td>
                                <td class="tls_header tls_border" style="text-align: left;"><b>Map</b></td>
                                <td class="tls_header tls_border" style="width: 8%;"><b>Time (seconds)</b></td>
                                <td class="tls_header tls_border" style="width: 8%;"><b>Finished (crossed)</b></td>
                            </tr>
                        <?php
                        foreach ($map->players as $record)
                        {
                            if (isset($record) && !is_null($record) && $record instanceof Record)
                            {
                                ?>
                            <tr class="tls_select">
                                <td class="tls_row tls_border" style="color: #ff3300;"><?php echo $record->rank; ?></td>
                                <td class="tls_row tls_border record_selection" style="text-align: left;"><a class="record_link" href="<?=Data::$data->url."index.php/Player/".$record->hash?>"><?=html_entity_decode($record->name)?></a></td>
                                <td class="tls_row tls_border" style="color: #aa33aa; width: 10%;"><?php echo $record->time; ?></td>
                                <td class="tls_row tls_border" style="color: #aa33aa;"><?php echo $record->played; ?></td>
                            </tr>
                            <?php
                            }
                        }
                        ?>
                        </table>
                        <?php
                    }
                    else
                    {
                        ?>
                        <div class="ranking_body" style="padding: 10px; font-weight: bold;">No players found...</div>
                    <?php
                    }
                    ?>
                    </div>
                </td>
            </tr>
        </table>
        <?php
    }
    else
    {
        loadHeaders();
        ?>
        <div class="player_body" style="padding: 10px; font-weight: bold;">Unknown map hash: "<?php echo $map_hash; ?>"</div>
    <?php
    }
}
?>