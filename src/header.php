<?php

function loadHeaders($param = "")
{
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <?php
        if (Filter($param) == "")
            $param = $param.Data::$data->area;

        echo "<title>".html_entity_decode($param).' &#8212; LOVER$BOY\'s Racing</title>'."\n";
        ?>
        <link type="text/css" href="<?php echo Data::$data->url; ?>main.css" rel="stylesheet"/>
        <script type="text/javascript">
            var url = "<?php echo Data::$data->url; ?>"
        </script>
        <script src="<?php echo Data::$data->url; ?>script.js"></script>
        <link rel="shortcut icon" href="<?php echo Data::$data->url; ?>images/favicon.ico" />
        <link rel="icon" type="image/png" href="<?php echo Data::$data->url; ?>images/favicon.png" />
    </head>
    <body>
        <h1 class="logo"><span style="color: #0099ff; font-size: 22px;">LOVER</span><span style="color: #88ff22; font-size: 22px;">$</span><span style="color: #ff00ff; font-size: 22px;">BOY</span><span style="color: #ff7700; font-size: 22px;">s </span><span style="color: #77ffff; font-size: 22px;">Racing</span></h1>
        <br />
        <div class="header"><?php headerLinks(); ?></div>
        <br />
        <?php
        if (Data::$data->under_work)
        {
            ?>
        <div class="notice"><div class="notice_msg"><img src="<?php echo Data::$data->url; ?>images/working.gif" /> The website is under construction...</div></div>
        <br />
            <?php
        }
        ?>
        <div class="content">
<?php
}

function headerLinks()
{
    ?>
    <a class="header_link <?php if (Filter(Data::$data->area) == "home") echo "header_link_current"; ?>" href="<?php echo Data::$data->url."index.php/Home"; ?>">HOME</a>
    <a class="header_link <?php if (Filter(Data::$data->area) == "news") echo "header_link_current"; ?>" href="<?php echo Data::$data->url."index.php/News"; ?>">NEWS</a>
    <a class="header_link <?php if (Filter(Data::$data->area) == "rankings") echo "header_link_current"; ?>" href="<?php echo Data::$data->url."index.php/Rankings"; ?>">RANKINGS</a>
    <a class="header_link <?php if (Filter(Data::$data->area) == "highscores") echo "header_link_current"; ?>" href="<?php echo Data::$data->url."index.php/Highscores"; ?>">HIGHSCORES</a>
    <a class="header_link <?php if ((Filter(Data::$data->area) == "players") || (Filter(Data::$data->area) == "player")) echo "header_link_current"; ?>" href="<?php echo Data::$data->url."index.php/Players"; ?>">PLAYERS</a>
    <a class="header_link <?php if ((Filter(Data::$data->area) == "maps") || (Filter(Data::$data->area) == "map")) echo "header_link_current"; ?>" href="<?php echo Data::$data->url."index.php/Maps"; ?>">MAPS</a>
    <?php
}

function loadFooters()
{
?>
        </div>
        <script>
            document.getElementById('filter').addEventListener('keyup', function () {
                var filterText = this.value,
                    lis = document.querySelectorAll('tr'),
                    x;

                for (x = 2; x < lis.length; x++) {
                    if (lis[x].childNodes[1].innerHTML.toLowerCase().indexOf(filterText.toLowerCase()) > -1) {
                        lis[x].removeAttribute('hidden');
                    }
                    else {
                        lis[x].setAttribute('hidden', true);
                    }
                }
            });
        </script>
    </body>
</html>
<?php
    Data::$data->sql->close();
    Data::$data->ftp->close();
}
?>