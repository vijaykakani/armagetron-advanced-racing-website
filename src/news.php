<?php

//  show a limited amount of news
function displayLimitedNews($show = 3)
{
    ?>
    <div class="title_header"><span class="title_header_txt">NEWS</span> <img title="News" style="float: right;" src="<?php echo Data::$data->url; ?>/images/icons/news.png" /></div>
    <?php

    $news_result = Data::$data->sql->query("SELECT * FROM `news`  ORDER BY `post_time` DESC LIMIT 0, ".$show);
    if ($news_result->num_rows > 0)
    {
        while ($row = $news_result->fetch_assoc())
        {
            ?>
            <div class="news_body">
                <table width="100%">
                    <tr>
                        <td class="news_id"><b>ID: <?php echo $row['news_id']; ?></b></td>
                        <td style="text-align: right;">
                            <b>
                                <?php
                                if (isset(Data::$data->phpbb_db))
                                {
                                    $result = Data::$data->phpbb_db->sql_query('SELECT * FROM `'.Data::$data->phpbb_table_prefix.'users` WHERE `user_id`="'.$row['user_id'].'"');
                                    $proces = mysql_fetch_assoc($result);
                                }
                                ?>
                                <span class="news_name"><?php echo @$proces['username']; ?></span>
                                <br />
                                <span class="news_date" style="color: #0055ff;"><?php echo date("F d, Y [h:i A]", $row['post_time']); ?></span>
                            </b>
                        </td>
                    </tr>
                </table>
                <hr />
                <br />
                <span style="padding: 3px; white-space: pre-wrap;"><?php echo html_entity_decode($row['post_body'])."<br />\n"; ?></span>
            </div>
        <?php
        }
    }
    else
    {
        ?>
        <div class="news_body">
            <b>There are no news to show...</b>
        </div>
    <?php
    }
}

//  show all the stored news
function loadNews()
{
    loadHeaders();
    ?>
    <div style="border: 1px solid #AAAAAA;"><?php displayNews(); ?></div>
    <?php
}

function displayNews()
{
    ?>
    <div class="title_header"><span class="title_header_txt">NEWS</span> <img title="News" style="float: right;" src="<?php echo Data::$data->url; ?>/images/icons/news.png" /></div>
    <?php

    $news_result = Data::$data->sql->query("SELECT * FROM `news`  ORDER BY `post_time` DESC");
    if ($news_result->num_rows)
    {
        while ($row = $news_result->fetch_assoc())
        {
            ?>
            <div class="news_body">
                <table width="100%">
                    <tr>
                        <td class="news_id"><b>ID: <?php echo $row['news_id']; ?></b></td>
                        <td style="text-align: right;">
                            <b>
                                <?php
                                if (isset(Data::$data->phpbb_db))
                                {
                                    $result = Data::$data->phpbb_db->sql_query('SELECT * FROM `'.Data::$data->phpbb_table_prefix.'users` WHERE `user_id`="'.$row['user_id'].'"');
                                    $proces = mysql_fetch_assoc($result);
                                }
                                ?>
                                <span class="news_name"><?php echo @$proces['username']; ?></span>
                                <br />
                                <span class="news_date" style="color: #0055ff;"><?php echo date("F d, Y [h:i A]", $row['post_time']); ?></span>
                            </b>
                        </td>
                    </tr>
                </table>
                <hr />
                <br />
                <span style="padding: 3px; white-space: pre-wrap;"><?php echo html_entity_decode($row['post_body'])."<br />\n"; ?></span>
            </div>
        <?php
        }
    }
    else
    {
        ?>
        <div class="news_body">
            <b>There are no news to show...</b>
        </div>
    <?php
    }
}