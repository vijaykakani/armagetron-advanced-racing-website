<?php

class Data
{
    static $data = null;

    var $url  = ""; //  url to current website
    var $root = ""; //  main path to script
    var $logs = ""; //  path to where /var/ files will be stored

    var $info = ""; //  info of the path being called
    var $area = ""; //  the area currently visiting

    var $under_work = false;    //  flag to indicate the website is under construction

    var $rec_path  = "";        //  path to where records are stored
    var $res_path  = "";        //  path to where maps are stored
    var $resources = array();   //  list of AUTHOR/CATEGORY for record identification

    var $sql = null;    //  hold the mysqli data connection
    var $ftp = null;    //  hold the ftp class data

    //  PHPBB Hack
    var $phpbb_user         = null;
    var $phpbb_auth         = null;
    var $phpbb_db           = null;
    var $phpbb_table_prefix = 'phpbb_';

    function __construct()
    {
        self::$data = $this;
    }
};

$root = __DIR__."/";
require $root."src/string.php";
require $root."src/color.php";
require $root."src/map_viewer.php";
require $root."src/identicon.php";
require $root."src/player.php";
require $root."src/map.php";
require $root."src/ftp.php";
require $root."src/header.php";
require $root."src/records.php";
require $root."src/news.php";
require $root."src/online.php";
require $root."src/home.php";
require $root."src/chat.php";

/*
//  PHPBB Hack
define('IN_PHPBB', true);
$phpbb_root_path = dirname($root)."/vertrex_public/forums/";
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);

// Start session management
$user->session_begin();
$auth->acl($user->data);
$user->setup();
*/

set_time_limit(300);

Data::$data = new Data();

Data::$data->under_work = true;

Data::$data->url  = "http://localhost/scripts/tron/scripts/games/aa_racing_website/";
Data::$data->root = $root;
Data::$data->logs = $root."logs/";

Data::$data->rec_path  = addslashes($root."/race_scores");
Data::$data->res_path  = addslashes($root."/maps");
Data::$data->resources = array
(
    "Angel/maps",
    "Animuson/maps",
    "BigTronDaddy/maps",
    "Blaze/maps",
    "Dakun/maps",
    "DarkChaos/maps",
    "Durf/maps",
    "Eggs/maps",
    "Eristan/maps",
    "Fire/maps",
    "Green/maps",
    "Gummi/maps",
    "Gunner/maps",
    "Hoop/maps",
    "INW/maps",
    "LoverBoy/maps",
    "MrO/maps",
    "Paralyzed/maps",
    "PuffyFluff/maps",
    "Slash/maps",
    "Smart/maps",
    "Smoothice/maps",
    "Soapy/maps",
    "Soska/maps",
    "TestSubject/maps",
    "Tsugaru/maps",
    "Unknown/maps",
    "VOV/maps",
    "Wrtlprnft/RaceMapGenerator",
    "Wrtlprnft/SubMaps",
    "Xyron/maps",
    //"Zwazi/maps",
    'Lover-Boy/Advanced/Racing',
    'StyX/racing',
    'Lover-Boy/racing',
    'turtle/adventure',
    'RePhoenix/Racing',
);

Data::$data->sql = new mysqli("localhost", "root", "", "vertrex_racing");
/*
Data::$data->ftp = new Ftp("aa.vertrex.org");
Data::$data->ftp->login("vertrex", "gjUre8Hf");
*/

session_start();
session_set_cookie_params(0);
session_cache_expire("private");

Data::$data->info = sanitize(@$_SERVER["PATH_INFO"]);
if (Filter(Data::$data->info) == "")
    Data::$data->area = "Home";
else
{
    Data::$data->info = explode("/", substr(Data::$data->info, 1));
    Data::$data->area = Data::$data->info[0];
    if (Filter(Data::$data->area) == "")
        Data::$data->area = "Home";
}

/*
Data::$data->phpbb_user = @$user;
Data::$data->phpbb_auth = @$auth;
Data::$data->phpbb_db   = @$db;
Data::$data->phpbb_table_prefix = 'phpbb_';
*/