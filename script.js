function loadRankingsYear(oThis)
{
    document.location = url + "index.php/Rankings/YEAR/" + oThis.value;
}

function loadRankingsOrd(oThis, type)
{
    document.location = url + "index.php/Rankings/YEAR/" + type + "/ORD/" + oThis.value;
}

function loadHighScoresYear(oThis)
{
    document.location = url + "index.php/Highscores/YEAR/" + oThis.value;
}

function loadHighScoresOrd(oThis, type)
{
    document.location = url + "index.php/Highscores/YEAR/" + type + "/ORD/" + oThis.value;
}

function loadPlayersOrd(oThis)
{
    document.location = url + "index.php/Players/ORD/" + oThis.value;
}

function loadMapsOrd(oThis)
{
    document.location = url + "index.php/Maps/ORD/" + oThis.value;
}

function loadMapsComp(oThis)
{
    document.location = url + "index.php/Maps/COMPLETE/" + oThis.value;
}

function loadPlayerYear(oThis, hash)
{
    document.location = url + "index.php/Player/" + hash + "/YEAR/" + oThis.value;
}

function loadPlayerOrd(oThis, hash, type)
{
    document.location = url + "index.php/Player/" + hash + "/YEAR/" + type + "/ORD/" + oThis.value;
}

function loadMapYear(oThis, hash)
{
    document.location = url + "index.php/Map/" + hash + "/YEAR/" + oThis.value;
}

function loadMapOrd(oThis, hash, type)
{
    document.location = url + "index.php/Map/" + hash + "/YEAR/" + type + "/ORD/" + oThis.value;
}