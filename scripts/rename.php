<?php
require dirname(__DIR__)."/base.php";
loadHeaders("Rename Player");

?>
<div class="title_header"><span class="title_header_txt">RENAME RECORDS</span></div>
<div class="ranking_body" style="padding: 10px; font-weight: bold;">
<?php

if (isset($_POST['rename_player']))
{
    $old_name = $_POST['old_name'];
    $new_name = $_POST['new_name'];

    if ((Filter($old_name) == "") || (Filter($new_name) == ""))
        echo "<b>You need to have the boxes filled in to work!</b>\n";
    else
    {
        echo $old_name." >> ".$new_name."<br />\n";

        foreach (Data::$data->resources as $resource)
        {
            if (!is_dir(Data::$data->rec_path."/".$resource)) continue;

            $list_of_recs = scandir(Data::$data->rec_path."/".$resource);
            foreach ($list_of_recs as $record)
            {
                if (($record == ".") || ($record == "..") || (!is_file(Data::$data->rec_path."/".$resource."/".$record))) continue;

                $lines = file_get_contents(Data::$data->rec_path."/".$resource."/".$record);

                if (!preg_match("~\b".$old_name."\b~",$lines))
                    continue;

                $lines = preg_replace("~\b".$old_name."\b~", $new_name, $lines);
                echo "Done: ".$resource."/".$record."<br />\n";
                echo "<pre>";
                echo htmlentities($lines);
                echo "</pre><br />";
                file_put_contents(Data::$data->rec_path."/".$resource."/".$record, $lines);
            }
        }
    }
}
else
{
    ?>
        <form action="" method="post">
            Old name: <input type="text" name="old_name" />
            New name: <input type="text" name="new_name" />
            <input type="submit" name="rename_player" />
        </form>
<?php
}
?>
</div>
<?php
loadFooters();
?>