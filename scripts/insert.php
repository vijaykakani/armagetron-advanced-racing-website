<?php
require dirname(__DIR__)."/base.php";

if (isset($_POST['insert_maps']))
{
    $maps_list = explode("\n", @$_POST['maps_list']);
    if (count($maps_list) > 0)
    {
        foreach ($maps_list as $map)
        {
            //  skip if map doesn't exist
            if (!file_exists(Data::$data->res_path."/".$map))
                continue;

            $map_dom = new DOMDocument();
            $map_dom->load(Data::$data->res_path."/".$map);

            $resources = $map_dom->getElementsByTagName("Resource");
            foreach ($resources as $resource)
            {
                $author   = $resource->getAttribute("author");
                $version  = $resource->getAttribute("version");
                $category = $resource->getAttribute("category");
                $name     = $resource->getAttribute("name");

                break;
            }
        }
    }
}
?>
<form action="" method="post">
    <textarea name="maps_list" cols="100" rows="35" style="width: 100%"><?=@$_POST['maps_list']?></textarea>
    <input type="submit" name="insert_maps" value="Insert Maps" style="font-size: large; padding: 7px;" />
</form>