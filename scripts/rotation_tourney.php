<?php
require dirname(__DIR__)."/base.php";
loadHeaders("Rotation");

$maps = array();
$maps_result = Data::$data->sql->query('SELECT * FROM `rotation` WHERE `map_difficulty`!="4"');
?>
    <div class="title_header"><span class="title_header_txt">MAP ROTATION</span></div>
    <div class="ranking_body" style="padding: 10px;">
        <b>Map Rotation: (<?php echo 100; ?> maps)</b>
        <table style="height: 500px; width: 100%; border: 1px solid #AAAAAA;">
            <tr>
                <td style="vertical-align: top; word-break: break-all; padding: 10px;">
                    <div style="overflow: auto; height: 100%; width: 100%;">
                        <?php
                        while ($row = $maps_result->fetch_assoc())
                        {
                            $map = new Map($row['map_rotation_id'], "", $row['map_name'], $row['map_author'], $row['map_version'], $row['map_category']);
                            $map->map_file = $row['map_file'];
                            $maps[] = $map;
                        }

                        echo "MAP_ROTATION ";
                        $i = 0;
                        while ($i < 100)
                        {
                            $key = rand(0, count($maps) - 1);
                            $map = $maps[$key];

                            if (isset($map) && !is_null($map) && $map instanceof Map && !$map->set)
                            {
                                echo $map->map_file.";";
                                $map->set = true;

                                $i++;
                            }
                        }
                        ?>
                    </div>
                </td>
            </tr>
        </table>
        <?php
        $key = array_rand($maps);
        ?>
        <div class="display_view"><b>MAP_FILE <?php $map = $maps[$key]; if (isset($map) && !is_null($map) && $map instanceof Map) echo $map->map_file; ?></b></div>
        <br />
        <?php
        $maps = array();
        $maps_result = Data::$data->sql->query('SELECT * FROM `rotation` WHERE `map_difficulty`="4"');
        ?>
        <b>Map Storage: (<?php echo $maps_result->num_rows; ?> maps)</b>
        <table style="height: 500px; width: 100%; border: 1px solid #AAAAAA;">
            <tr>
                <td style="vertical-align: top; word-break: break-all; padding: 10px;">
                    <div style="overflow: auto; height: 100%; width: 100%;">
                        <?php
                        while ($row = $maps_result->fetch_assoc())
                        {
                            $map = new Map($row['map_rotation_id'], "", $row['map_name'], $row['map_author'], $row['map_version'], $row['map_category']);
                            $map->map_file = $row['map_file'];
                            $maps[] = $map;
                        }

                        echo "MAP_STORAGE ";
                        $i = 1;
                        while ($i < $maps_result->num_rows)
                        {
                            $key = rand(0, count($maps) - 1);
                            $map = $maps[$key];

                            if (isset($map) && !is_null($map) && $map instanceof Map && !$map->set)
                            {
                                echo $map->map_file.";";
                                $map->set = true;

                                $i++;
                            }
                        }
                        ?>
                    </div>
                </td>
            </tr>
        </table>
    </div>
<?php
loadFooters();
?>