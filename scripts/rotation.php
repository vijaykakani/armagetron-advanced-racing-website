<?php
require dirname(__DIR__)."/base.php";
loadHeaders("Rotation");

$maps = array();

$maps_result = Data::$data->sql->query('SELECT * FROM `rotation` WHERE `map_difficulty`!="4"');
?>
    <div class="title_header"><span class="title_header_txt">MAP ROTATION</span></div>
    <div class="ranking_body" style="padding: 10px;">
        <b>Map Rotation: (<?php echo $maps_result->num_rows; ?> maps)</b>
        <table style="height: 500px; width: 100%; border: 1px solid #AAAAAA;">
            <tr>
                <td style="vertical-align: top; word-break: break-all; padding: 10px;">
                    <div style="overflow: auto; height: 100%; width: 100%;">
                        <?php
                        while ($row = $maps_result->fetch_assoc())
                            $maps[] = $row['map_file'];

                        echo "MAP_ROTATION ";
                        shuffle($maps);
                        echo implode(';', $maps);


                        ?>
                    </div>
                </td>
            </tr>
        </table>
        <div class="display_view"><b>MAP_FILE <?=$maps[array_rand($maps, 1)]?></b></div>
        <br />
        <?php
        $maps = array();
        $maps_result = Data::$data->sql->query('SELECT * FROM `rotation` WHERE `map_difficulty`="4"');
        ?>
        <b>Map Storage: (<?php echo $maps_result->num_rows; ?> maps)</b>
        <table style="height: 500px; width: 100%; border: 1px solid #AAAAAA;">
            <tr>
                <td style="vertical-align: top; word-break: break-all; padding: 10px;">
                    <div style="overflow: auto; height: 100%; width: 100%;">
                        <?php
                        while ($row = $maps_result->fetch_assoc())
                            $maps[] = $row['map_file'];

                        echo "MAP_STORAGE ";
                        shuffle($maps);
                        echo implode(';', $maps);
                        ?>
                    </div>
                </td>
            </tr>
        </table>
    </div>
<?php
loadFooters();
?>