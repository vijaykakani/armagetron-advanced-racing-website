<?php
require dirname(__DIR__)."/base.php";
loadHeaders("Update");

$year = 2019;
$race_scores = Data::$data->root."/new_race_scores";

?>
<div class="title_header"><span class="title_header_txt">UPDATE RECORDS - <?=$year?></span></div>
<div class="ranking_body" style="padding: 10px; font-weight: bold;">
<?php

//DownloadRecords();

if (count(Data::$data->resources) > 0)
{
    Data::$data->sql->query("TRUNCATE TABLE `maps`");

    echo "LOADING MAPS...<br />\n";
    $rotationResult = Data::$data->sql->query('SELECT * FROM `rotation`');
    while ( $rotation = $rotationResult->fetch_assoc() )
    {
        $name       = $rotation['map_name'];
        $author     = $rotation['map_author'];
        $version    = $rotation['map_version'];
        $category   = $rotation['map_category'];
        $difficulty = $rotation['map_difficulty'];

        $map_file   = $rotation['map_file'];
        $map_hash   = md5($map_file.".txt");

        MapViewer(Data::$data->res_path, $map_file, Data::$data->root."/images/maps", $map_hash, '100%');

        $exists = 0;
        if (file_exists($race_scores."/".$map_file.".txt"))
            $exists = 1;

        $query = 'INSERT INTO `maps`(`map_author`, `map_version`, `map_name`, `map_category`, `map_file`, `map_hash`, `map_exists` , `map_difficulty`) VALUES ("'.
            $author.'", "'.$version.'", "'.$name.'", "'.$category.'", "'.$map_file.'", "'.$map_hash.'", "'.$exists.'", "'.$difficulty.'")';

        if (!Data::$data->sql->query($query))
            die(Data::$data->sql->connect_error);
    }
}

if (is_dir($race_scores))
{
    echo "LOADING PLAYERS...<br >\n";
    //Data::$data->sql->query("TRUNCATE TABLE  `players`");
    foreach (Data::$data->resources as $resource)
    {
        if (!is_dir($race_scores."/".$resource)) continue;

        $list_of_recs = scandir($race_scores."/".$resource);
        foreach ($list_of_recs as $record)
        {
            if (($record == ".") || ($record == "..") || (!is_file($race_scores."/".$resource."/".$record))) continue;

            $dataStr = htmlentities(file_get_contents($race_scores."/".$resource."/".$record));
            $dataStr = explode("\n", $dataStr);

            foreach ($dataStr as $line)
            {
                if ($line == "") continue;

                $ext = explode(" ", $line);

                $result = Data::$data->sql->query('SELECT * FROM `players` WHERE `player_hash`="'.md5($ext[0]).'"');
                if ($result->num_rows == 0)
                {
                    $query = 'INSERT INTO `players`(`player_name`, `player_hash`) VALUES ("'.htmlentities($ext[0]).'", "'.md5($ext[0]).'")';
                    if (!Data::$data->sql->query($query))
                        die(Data::$data->sql->connect_error);
                }
            }
        }
    }

    echo "LOADING MAP RECORDS:<br >\n";
    Data::$data->sql->query('DELETE FROM `records` WHERE `year`="'.$year.'"');
    ?>
    <table cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="tls_header" style="text-align: left; border: 1px solid #BBBBBB;"><b>MAP_FILE</b></td>
            <td class="tls_header" style="text-align: right; width: 10%; border: 1px solid #BBBBBB;"><b>Completed</b></td>
        </tr>
    <?php
    foreach (Data::$data->resources as $resource)
    {
        if (!is_dir($race_scores."/".$resource)) continue;

        $list_of_scores = scandir($race_scores."/".$resource);
        foreach ($list_of_scores as $record)
        {
            if (($record == ".") || ($record == "..") || (!is_file($race_scores."/".$resource."/".$record))) continue;

            $map_file = $resource."/".$record;
            $dataStr = htmlentities(file_get_contents($race_scores."/".$map_file));
            $dataStr = explode("\n", $dataStr);

            $rank = 0;

            ?>
        <tr class="tls_select">
            <td class="tls_row" style="text-align: left; border: 1px solid #BBBBBB; padding: 10px; color: #0000ff;"><?php echo $map_file; ?></td>
            <?php

            $map_res = Data::$data->sql->query('SELECT * FROM `maps` WHERE `map_hash`="'.md5($resource."/".$record).'"');
            if ($map_res->num_rows == 1)
            {
                $map_row = $map_res->fetch_assoc();

                foreach ($dataStr as $line)
                {
                    if (trim($line) == "") continue;
                    $rank++;

                    $ext = explode(" ", $line);

                    $player_res = Data::$data->sql->query('SELECT * FROM `players` WHERE `player_hash`="'.md5($ext[0]).'"');
                    $player_row = $player_res->fetch_assoc();

                    $query = 'INSERT INTO `records`(`map_id`, `player_id`, `ranks`, `times`, `finished`, `year`) VALUES ("'.$map_row['map_id'].'", "'.$player_row['player_id'].'", "'.$rank.'", "'.$ext[1].'", "'.$ext[2].'", "'.$year.'")';
                    if (!Data::$data->sql->query($query))
                        die(Data::$data->sql->connect_error);
                }

                ?>
            <td class="tls_row" style="text-align: center; width: 10%; border: 1px solid #BBBBBB; padding: 10px; color: #007700;">Success</td>
                <?php
            }
            else
            {
                ?>
            <td class="tls_row" style="text-align: center; width: 10%; border: 1px solid #BBBBBB; padding: 10px; color: #ff3300">Failed</td>
                <?php
            }
            ?>
        </tr>
            <?php
        }
    }
    ?>
    </table>
    <?php
}
?>
</div>
<?php

function DownloadRecords()
{
    $rec_root = "/home/vertrex/normal/servers/racing_area1/var/race_scores";

    if (count(Data::$data->resources) > 0)
    {
        if (!is_dir(Data::$data->rec_path))
            mkdir(Data::$data->rec_path);

        foreach (Data::$data->resources as $resource)
        {
            if (!is_dir(Data::$data->rec_path."/".$resource))
            {
                $dirExt = explode("/", $resource);
                $dirs = Data::$data->rec_path;
                foreach ($dirExt as $dir)
                {
                    $dirs .= "/".$dir;
                    if (!is_dir($dirs))
                        mkdir($dirs);
                }
            }

            $dir_contents = Data::$data->ftp->dir_list($rec_root."/".$resource);
            if ($dir_contents !== false)
            {
                foreach ($dir_contents as $content)
                {
                    if (($content == ".") || ($content == "..")) continue;

                    $content = basename($content);

                    Data::$data->ftp->download(Data::$data->rec_path."/".$resource."/".$content, $rec_root."/".$resource."/".$content, FTP_ASCII);
                }
            }
        }
    }
}

loadFooters();
?>