<?php
require dirname(__DIR__)."/base.php";
loadHeaders("Reload");

echo "<pre style='word-wrap: break-word;'>\n";

foreach (Data::$data->resources as $resource)
{
    $files = scandir(Data::$data->rec_path."/".$resource);
    foreach ($files as $file)
    {
        $path = Data::$data->rec_path."/".$resource."/".$file;
        if (!is_file($path))
            continue;

        echo $resource."/".$file."\n\n";

        //$contents = file_get_contents('ftp://vertrex:gjUre8Hf@aa.vertrex.org/home/vertrex/normal/servers/racing_area1/var/race_scores/'.);
        $contents = htmlentities(file_get_contents($path));
        if (trim($contents) == '') continue;
        //echo "CONSOLE_MESSAGE 0x009900CONTENT: 0x00ff00TRUE\n";

        echo $contents."\n\n";

        $data   = urlencode($contents);
        $n_data = urldecode($contents);

        echo $data."\n\n";
        echo $n_data."\n\n";

        echo "<hr />\n";
    }
}