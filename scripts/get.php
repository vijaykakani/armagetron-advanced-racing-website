<?php
require dirname(__DIR__)."/base.php";

if (!isset($_REQUEST['year']) || !isset($_REQUEST['map']) || !isset($_REQUEST['data']))
    die("Nanananana!!");

$year = @$_REQUEST['year'];
$map  = @$_REQUEST['map'];
$data = @$_REQUEST['data'];

$race = explode("\n", htmlentities(urldecode($data)));

$exists = 1;

$path = Data::$data->res_path."/".$map;

$rotationResult = Data::$data->sql->query('SELECT * FROM `rotation` WHERE `map_file`="'.$map.'"');
if (!$rotationResult)
    die(Data::$data->sql->connect_error);
if ($rotationResult->num_rows == 0)
    die("No good. Map doesn't exist.");
$rotation = $rotationResult->fetch_assoc();

$name       = $rotation['map_name'];
$author     = $rotation['map_author'];
$version    = $rotation['map_version'];
$category   = $rotation['map_category'];
$difficulty = $rotation['map_difficulty'];

$map_file   = $map;
$map_hash   = md5($map_file.".txt");

$mapResult =  Data::$data->sql->query('SELECT * FROM `maps` WHERE `map_hash`="'.$map_hash.'"');
if (!$mapResult)
    die(Data::$data->sql->connect_error);

MapViewer(Data::$data->res_path, $map_file, Data::$data->root."/images/maps", $map_hash, '100%');

//  Create the map record if it doesn't exist
if ($mapResult->num_rows == 0)
{
    $query = 'INSERT INTO `maps`(`map_author`, `map_version`, `map_name`, `map_category`, `map_file`, `map_hash`, `map_exists` , `map_difficulty`) VALUES ("'.
        $author.'", "'.$version.'", "'.$name.'", "'.$category.'", "'.$map_file.'", "'.$map_hash.'", "'.$exists.'", "'.$difficulty.'")';
    Data::$data->sql->query($query);
}
else
{
    $map_process = $mapResult->fetch_assoc();
    $query = 'UPDATE `maps` SET `map_exists`="'.$exists.'" WHERE `map_hash`="'.$map_hash.'"';
    Data::$data->sql->query($query);
}

//  Add players into db if they don't already exist
foreach ($race as $line)
{
    if ($line == "") continue;

    $ext = explode(" ", $line);

    $result = Data::$data->sql->query('SELECT * FROM `players` WHERE `player_hash`="'.md5($ext[0]).'"');
    if ($result->num_rows == 0)
    {
        $query = 'INSERT INTO `players`(`player_name`, `player_hash`) VALUES ("'.$ext[0].'", "'.md5($ext[0]).'")';
        if (!Data::$data->sql->query($query))
            die(Data::$data->sql->connect_error);
    }
}

$rank = 0;
$map_res = Data::$data->sql->query('SELECT * FROM `maps` WHERE `map_hash`="'.$map_hash.'"');
$map_row = $map_res->fetch_assoc();
Data::$data->sql->query('DELETE FROM `records` WHERE `map_id`="'.$map_row['map_id'].'" AND `year`="'.$year.'"');
foreach ($race as $line)
{
    if (trim($line) == "") continue;
    $rank++;

    $ext = explode(" ", $line);

    $player_res = Data::$data->sql->query('SELECT * FROM `players` WHERE `player_hash`="' . md5($ext[0]) . '"');
    $player_row = $player_res->fetch_assoc();

    $query = 'INSERT INTO `records`(`map_id`, `player_id`, `ranks`, `times`, `finished`, `year`) VALUES ("'.$map_row['map_id'].'", "'.$player_row['player_id'].'", "'.$rank.'", "'.$ext[1].'", "'.$ext[2].'", "'.$year.'")';
    if (!Data::$data->sql->query($query))
        die(Data::$data->sql->connect_error);
}