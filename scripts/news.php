<?php
require dirname(__DIR__)."/base.php";
loadHeaders("Manage News");

?>
    <div class="title_header"><span class="title_header_txt">UPDATE RECORDS</span></div>
    <div class="tls_body" style="padding: 10px; font-weight: bold;">
        <form action="" method="post">
        <?php
        if (isset($_POST["add"]))
        {
            $body = sanitize(@$_POST["news_body"]);
            if ($body != "")
            {
                $body = htmlentities($body);
                $time = time();

                $user_id = 1;
                if (Data::$data->phpbb_user->data)
                    $user_id = Data::$data->phpbb_user->data['user_id'];

                $query = 'INSERT INTO `news`(`post_time`, `post_body`, `user_id`) VALUES ("'.$time.'", "'.$body.'", "'.$user_id.'")';
                Data::$data->sql->query($query);

                echo "News Added Successfully!";
            }
        }
        elseif (isset($_POST["edit"]))
        {
            $id = intval(sanitize($_POST["edit_id"]));
            if ($id > 0)
            {
                $result = Data::$data->sql->query('SELECT * FROM `news` WHERE `news_id`="'.$id.'"');
                if ($result->num_rows == 1)
                {
                    $row = $result->fetch_row();
                    ?>
                    EDIT NEWS ID ( <?php echo $id; ?> )
                    <table style="color: #f9f9f9; width: 100%; text-align: center; text-align: left;">
                        <tr>
                            <td style="vertical-align: top;"><textarea style="width: 100%;" name="news_update_body" rows="23"><?php echo html_entity_decode($row[2], ENT_COMPAT | ENT_HTML401 | ENT_SUBSTITUTE, "ISO-8859-1"); ?></textarea></td>
                        </tr>
                        <tr>
                            <td><input name="update" type="submit" value="Update News"/></td>
                        </tr>
                        <tr>
                            <td><input name="update_id" type="hidden" value="<?php echo $id; ?>" /></td>
                        </tr>
                    </table>
                    <hr style="border-color: #ff0000;" />
                <?php
                }

                loadFooters();
                die();
            }
        }
        elseif (isset($_POST["update"]))
        {
            $id   = sanitize($_POST['update_id']);
            $body = sanitize($_POST["news_update_body"]);

            if (trim($body) != "")
            {
                $result = Data::$data->sql->query('SELECT * FROM `news` WHERE `news_id`="'.$id.'"');
                if ($result->num_rows == 1)
                {
                    $query = 'UPDATE `news` SET `post_body`="'.htmlentities($body, ENT_COMPAT | ENT_HTML401 | ENT_SUBSTITUTE, "ISO-8859-1").'" WHERE `news_id`="'.$id.'"';
                    if (!Data::$data->sql->query($query))
                        echo Data::$data->sql->connect_error;
                    else
                        echo "Update successful!.<br />\n";
                }
            }
        }
        ?>
            ADD NEWS
            <table style="color: #f9f9f9; width: 100%; text-align: center; text-align: left;">
                <tr>
                    <td><textarea style="width: 100%;" name="news_body" rows="15"></textarea></td>
                </tr>
                <tr>
                    <td><input name="add" type="submit" value="Add News"/></td>
                </tr>
            </table>
            <hr style="border-color: #ff0000;" />
            EDIT NEWS
            <table style="color: #f9f9f9; width: 100%; text-align: center; text-align: left;">
                <tr>
                    <td><input name="edit_id" type="text" /></td>
                    <td><input name="edit" type="submit" value="Edit News"/></td>
                </tr>
            </table>
        </form>
    </div>
<?php
loadFooters();
?>