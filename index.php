<?php
require "base.php";

switch (Filter(Data::$data->area))
{
    case "home":
        loadHome();
        break;

    case "news":
        loadNews();
        break;

    case "rankings":
        viewRankings();
        break;

    case "highscores":
        viewHighScores();
        break;

    case "players":
        listPlayers();
        break;

    case "maps":
        listMaps();
        break;

    case "player":
        viewPlayerRecords();
        break;

    case "map":
        viewMapRecords();
        break;

    /*
    case "chatlog":
        viewChatLog();
        break;
    */

    default:
        WrongPage();
        break;
}

loadFooters();